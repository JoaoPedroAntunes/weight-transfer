from typing import List

from Design.Inputs_D import Inputs_D
from Design.Vehicle_D import Vehicle_D
from Results.Vehicle_R import Vehicle_R
from Simulations import WeightTransfer
from Solver.Inputs_S import Inputs_S
from Solver.Vehicle_S import Vehicle_S


class WeightTransferSimulation:

    def __init__(self,
                 vehicle_S: List[Vehicle_D] = None,
                 inputs_S: List[Inputs_D] = None):

        self._simulation_results = []
        self.__vehicle_model_list__ = []
        self.__simulation_input_list__ = []

        if type(inputs_S) is list:
            for inp in inputs_S:
                self.__simulation_input_list__.append(Inputs_S(inp))
        else:
            self.__simulation_input_list__ = [Inputs_S(inputs_S)]

        if type(vehicle_S) is list:
            for veh in vehicle_S:
                self.__vehicle_model_list__.append(Vehicle_S(veh))
        else:
            self.__vehicle_model_list__ = [vehicle_S]

        # Validate input
        for inp in self.__simulation_input_list__:
            inp.__generate_simulation_steps__()
            inp.__set_input_array_to_same_size__()
            inp.__validated_input__()

    def append_vehicle_model(self, vehicle: Vehicle_S()):
        self.__vehicle_model_list__.append(vehicle)

    def append_simulation_input(self, simulation_input):
        self.__simulation_input_list__.append(simulation_input)

    def init_local_variables(self, a_vehicle_model: Vehicle_S(), a_simulation_input: Inputs_S()):

        self._ay = a_simulation_input.lateral_acceleration
        self._ax = a_simulation_input.longitudinal_acceleration
        self._vx = a_simulation_input.longitudinal_velocity

        # TODO: Finish this
        self._non_susp_mass_FL = a_vehicle_model.suspension.front.left.non_suspended_mass
        self._non_susp_mass_FR = a_vehicle_model.suspension.front.right.non_suspended_mass
        self._non_susp_mass_RL = a_vehicle_model.suspension.rear.left.non_suspended_mass
        self._non_susp_mass_RR = a_vehicle_model.suspension.rear.right.non_suspended_mass

        self._non_susp_mass_front_CG = a_vehicle_model.suspension.front.non_susp_mass_CG
        self._non_susp_mass_rear_CG = a_vehicle_model.suspension.rear.non_susp_mass_CG

        self._susp_mass = a_vehicle_model.calculate_suspended_mass()

        self._susp_mass_CG = a_vehicle_model.calculate_suspended_mass_CG()

        self._susp_mass_WD = a_vehicle_model.basic_properties.suspended_weight_distribution

        self._front_track = a_vehicle_model.suspension.front.track
        self._rear_track = a_vehicle_model.suspension.rear.track
        self._wheelbase = a_vehicle_model.basic_properties.wheelbase

        self._front_RC = a_vehicle_model.suspension.front.roll_center
        self._rear_RC = a_vehicle_model.suspension.rear.roll_center

        self._wheel_rate_no_tire_FL = a_vehicle_model.suspension.front.left.spring.calculate_wheel_rate()
        self._wheel_rate_no_tire_FR = a_vehicle_model.suspension.front.right.spring.calculate_wheel_rate()
        self._wheel_rate_no_tire_RL = a_vehicle_model.suspension.rear.left.spring.calculate_wheel_rate()
        self._wheel_rate_no_tire_RR = a_vehicle_model.suspension.rear.right.spring.calculate_wheel_rate()

        self._front_ARB_MR = a_vehicle_model.suspension.front.ARB.motion_ratio
        self._rear_ARB_MR = a_vehicle_model.suspension.rear.ARB.motion_ratio

        self._front_ARB_stiffness = a_vehicle_model.suspension.front.ARB.stiffness
        self._rear_ARB_stiffness = a_vehicle_model.suspension.rear.ARB.stiffness

        self._deltaZ = a_vehicle_model.calculate_distance_SM_CG_to_roll_axis()

        self._front_spring_anti_roll_moment = a_vehicle_model.suspension.front.calculate_spring_anti_roll_moment()
        self._front_ARB_anti_roll_moment = a_vehicle_model.suspension.front.calculate_ARB_anti_roll_moment()

        self._rear_spring_anti_roll_moment = a_vehicle_model.suspension.rear.calculate_spring_anti_roll_moment()
        self._rear_ARB_anti_roll_moment = a_vehicle_model.suspension.rear.calculate_ARB_anti_roll_moment()

        self._front_total_anti_roll_moment = self._front_spring_anti_roll_moment + self._front_ARB_anti_roll_moment
        self._rear_total_anti_roll_moment = self._rear_spring_anti_roll_moment + self._rear_ARB_anti_roll_moment
        self._total_anti_roll_moment = self._front_total_anti_roll_moment + self._rear_total_anti_roll_moment


    def calculate_total_weight_transfer(self):

        self._non_susp_mass_front_WT = WeightTransfer.calculate_non_suspended_mass_weight_transfer(self._non_susp_mass_FL +
                                                                                        self._non_susp_mass_FR,
                                                                                        self._ay,
                                                                                        self._non_susp_mass_front_CG,
                                                                                        self._front_track)

        self._non_susp_mass_rear_WT = WeightTransfer.calculate_non_suspended_mass_weight_transfer(self._non_susp_mass_RL +
                                                                                        self._non_susp_mass_RR,
                                                                                        self._ay,
                                                                                        self._non_susp_mass_rear_CG,
                                                                                        self._rear_track)

        self._geometric_WT_front = WeightTransfer.calculate_geometry_weight_transfer(self._susp_mass,
                                                                                     self._susp_mass_WD,
                                                                                     self._ay,
                                                                                     self._front_RC.z,
                                                                                     self._front_track)

        self._geometric_WT_rear = WeightTransfer.calculate_geometry_weight_transfer(self._susp_mass,
                                                                                    (1-self._susp_mass_WD),
                                                                                    self._ay,
                                                                                    self._rear_RC.z,
                                                                                    self._rear_track)

        self._elastic_WT_front = WeightTransfer.calculate_elastic_weight_transfer(self._susp_mass,
                                                                        self._ay,
                                                                        self._deltaZ,
                                                                        self._front_spring_anti_roll_moment,
                                                                        self._front_ARB_anti_roll_moment,
                                                                        self._total_anti_roll_moment,
                                                                        self._front_track)

        self._elastic_WT_rear = WeightTransfer.calculate_elastic_weight_transfer(self._susp_mass,
                                                                        self._ay,
                                                                        self._deltaZ,
                                                                        self._rear_spring_anti_roll_moment,
                                                                        self._rear_ARB_anti_roll_moment,
                                                                        self._total_anti_roll_moment,
                                                                        self._rear_track)

        self._total_elastic_WT = self._elastic_WT_front + self._elastic_WT_rear
        self._total_non_susp_mass_WT = self._non_susp_mass_front_WT + self._non_susp_mass_rear_WT
        self._total_geometric_WT = self._geometric_WT_front + self._geometric_WT_rear

    def save_simulation_results(self, res):
        self._simulation_results.append(res)


    def get_simulation_results(self):
        return self._simulation_results

    def run(self):

        for i in range(len(self.__vehicle_model_list__)):
            print("Running Vehicle Model: {}".format(i))

            for j in range(len(self.__simulation_input_list__)):
                print("     Running Simulation Input: {}".format(j))

                local_results = Vehicle_R(str(j), len(self.__simulation_input_list__[j].lateral_acceleration))

                for k in range(len(self.__simulation_input_list__[j].lateral_acceleration)):
                    print("         Running step: {}".format(k))
                    loc = Inputs_S()
                    loc.lateral_acceleration = self.__simulation_input_list__[j].lateral_acceleration[k]
                    loc.longitudinal_acceleration = self.__simulation_input_list__[j].longitudinal_acceleration[k]
                    loc.longitudinal_velocity = self.__simulation_input_list__[j].longitudinal_velocity[k]

                    self.init_local_variables(self.__vehicle_model_list__[i], loc)

                    self.calculate_total_weight_transfer()

                    local_results.append_simulation_step_results(self, k)

                # self.save_simulation_results(local_results)
                self.__vehicle_model_list__[i].results.append(local_results)

                print()

    print("Simulation Complete")

    def run_step(self, vehicle, a_input):

        self.init_local_variables(vehicle, a_input)
        self.calculate_total_weight_transfer()
