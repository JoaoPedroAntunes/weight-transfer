import os
from pathlib import Path

from Design.ARB_D import ARB_D
from Design.Aerodynamics_D import Aerodynamics_D
from Design.BasicProperties_D import BasicProperties_D
from Design.HalfSuspension_D import HalfSuspension_D
from Design.Inputs_D import Inputs_D
from Design.Spring_D import Spring_D
from Design.Tire_D import Tire_D
from Globals import Globals
from Managers.ProjectFramework import ProjectFramework
from UserInterface.MainWindow_W import MainWindow_W

if __name__ == "__main__":
    main_window_W = MainWindow_W()
    project_name = "Test"
    file_path = str(Path.home()) + "/Documents/test"

    # If folder doesn't exist create the folder and populate it with a demo project
    if not os.path.exists(file_path):
        foo = ProjectFramework("test", ".wt", "0.0.0")
        foo.create_project_folders(project_name, file_path)

        # Create 3 files for each folder
        name_list = ["Test1", "Test2", "Test3"]
        for name in name_list:
            aero = Aerodynamics_D()
            aero.name = name
            aero.init_default_values()
            aero.save_object(file_path + "\\" + Globals.TreeViewNames.aerodynamics.value)

            bp = BasicProperties_D()
            bp.name = name
            bp.init_default_values()
            bp.save_object(file_path + "\\" + Globals.TreeViewNames.basic_properties.value)

            inp = Inputs_D()
            inp.name = name
            inp.init_default_values()
            inp.save_object(file_path + "\\" + Globals.TreeViewNames.inputs.value)

            spring = Spring_D()
            spring.name = name
            spring.init_default_values()
            spring.save_object(file_path + "\\" + Globals.TreeViewNames.spring.value)

            arb = ARB_D()
            arb.name = name
            arb.init_default_values()
            arb.save_object(file_path + "\\" + Globals.TreeViewNames.ARB.value)

            tire = Tire_D()
            tire.name = name
            tire.init_default_values()
            tire.save_object(file_path + "\\" + Globals.TreeViewNames.tire.value)

            susp = HalfSuspension_D()
            susp.name = name
            susp.init_default_values()
            susp.save_object(file_path + "\\" + Globals.TreeViewNames.half_suspension.value)

    else:
        main_window_W.open_project(file_path + "/" + "Test.wt")

    main_window_W.show()
