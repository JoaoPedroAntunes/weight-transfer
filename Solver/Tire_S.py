from Design.Tire_D import Tire_D

#Todo Add a name as an input. Like we have in the design folder
class Tire_S(Tire_D):
    def __init__(self, tire_d=None):
        Tire_D.__init__(self)
        if tire_d is not None:
            self.stiffness = tire_d.stiffness

    def set_tire(self, tire_D : Tire_D):
        self.stiffness = tire_D.stiffness