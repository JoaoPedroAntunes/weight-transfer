from typing import List

from Design.Vehicle_D import Vehicle_D
from Results.Vehicle_R import Vehicle_R
from Solver.Aerodynamics_S import Aerodynamics_S
from Solver.BasicProperties_S import BasicProperties_S
from Solver.Suspension_S import Suspension_S


# Todo Add a name as an input. Like we have in the design folder
class Vehicle_S:
    def __init__(self, aVehicle_D : Vehicle_D = None):

        self.aerodynamics = Aerodynamics_S()
        self.basic_properties = BasicProperties_S()
        self.suspension = Suspension_S()
        self.results: List[Vehicle_R] = []

        if aVehicle_D is not None:
            self.aerodynamics = Aerodynamics_S(aVehicle_D.aerodynamics)
            self.basic_properties = BasicProperties_S(aVehicle_D.basic_properties)
            self.suspension = Suspension_S(aVehicle_D.suspension)

    def calculate_suspended_mass(self) -> float:
        weight = self.basic_properties.weight
        non_suspended_mass_FL = self.suspension.front.left.non_suspended_mass
        non_suspended_mass_FR = self.suspension.front.right.non_suspended_mass
        non_suspended_mass_RL = self.suspension.rear.left.non_suspended_mass
        non_suspended_mass_RR = self.suspension.rear.right.non_suspended_mass

        return weight - non_suspended_mass_FL - non_suspended_mass_FR - non_suspended_mass_RL - non_suspended_mass_RR

    def calculate_suspended_mass_CG(self) -> float:
        weight = self.basic_properties.weight
        suspended_weight_CG = self.basic_properties.suspended_weight_CG
        non_suspended_mass_FL = self.suspension.front.left.non_suspended_mass
        non_suspended_mass_FR = self.suspension.front.right.non_suspended_mass
        non_suspended_mass_RL = self.suspension.rear.left.non_suspended_mass
        non_suspended_mass_RR = self.suspension.rear.right.non_suspended_mass
        front_non_susp_mass_CG = self.suspension.front.non_susp_mass_CG
        rear_non_susp_mass_CG = self.suspension.rear.non_susp_mass_CG
        suspended_mass = self.calculate_suspended_mass()

        return (weight * suspended_weight_CG -
                ((non_suspended_mass_FL + non_suspended_mass_FR)*front_non_susp_mass_CG +
                                   (non_suspended_mass_RL + non_suspended_mass_RR)*rear_non_susp_mass_CG)) / suspended_mass

    def calculate_distance_SM_CG_to_roll_axis(self):
        susp_mass_CG = self.calculate_suspended_mass_CG()
        front_RC = self.suspension.front.roll_center
        rear_RC = self.suspension.rear.roll_center
        susp_mass_weight_distribution = self.basic_properties.suspended_weight_distribution
        wheelbase = self.basic_properties.wheelbase

        return susp_mass_CG - \
               (((rear_RC.z - front_RC.z) / wheelbase * ((1 - susp_mass_weight_distribution) * wheelbase)) + front_RC.z)

    def calculate_suspended_mass_weight_distribution(self) -> float:
        front_non_suspended_mass = self.suspension.front.left.non_suspended_mass + self.suspension.front.right.non_suspended_mass
        rear_non_suspended_mass = self.suspension.rear.left.non_suspended_mass + self.suspension.rear.right.non_suspended_mass

        return (front_non_suspended_mass / (front_non_suspended_mass + rear_non_suspended_mass))

    def calculate_non_suspended_mass_weight_distribution(self):
        suspended_mass = self.calculate_suspended_mass()
        weight_distribution = self.basic_properties.suspended_weight_distribution
        front_non_suspended_mass = self.suspension.front.left.non_suspended_mass + self.suspension.front.right.non_suspended_mass
        weight = self.basic_properties.weight

        return (weight * weight_distribution - front_non_suspended_mass) / suspended_mass

    def calculate_static_load(self):
        weight = self.basic_properties.weight
        weight_distribution = self.basic_properties.suspended_weight_distribution

        return [0.5 * weight * weight_distribution, 0.5 * weight * weight_distribution,
                0.5 * weight * (1 - weight_distribution), 0.5 * weight * (1 - weight_distribution)]

    def print_vehicle_properties(self):
        """
        Prints the current values loaded on the vehicle_D class
        """
        print("Aero:")
        print("Downforce Coefficient: {}".format(self.aerodynamics.downforce_coefficient))
        print("Frontal Area: {}".format(self.aerodynamics.frontal_area))
        print("Air Density: {}".format(self.aerodynamics.air_density))
        print("Downforce Distribution: {}".format(self.aerodynamics.downforce_distribution))
        print("")

        print("Basic Properties")
        print("Weight: {}".format(self.basic_properties.weight))
        print("Front Track: {}".format(self.suspension.front.track))
        print("Rear Track: {}".format(self.suspension.rear.track))
        print("Suspended CG Height: {}".format(self.basic_properties.suspended_weight_CG))
        print("Weight Distribution: {}".format(self.basic_properties.suspended_weight_distribution))
        print("")

        # Suspension spring stiffness
        print("Suspension spring stiffness:")
        print("FL Spring Stiffness: {}".format(self.suspension.front.left.spring.stiffness))
        print("FR Spring Stiffness: {}".format(self.suspension.front.right.spring.stiffness))
        print("RL Spring Stiffness: {}".format(self.suspension.rear.left.spring.stiffness))
        print("RR Spring Stiffness: {}".format(self.suspension.rear.right.spring.stiffness))
        print("")

        # Suspension spring motion ratio
        print("Suspension spring MR:")
        print("FL Spring MR: {}".format(self.suspension.front.left.spring.motion_ratio))
        print("FR Spring MR: {}".format(self.suspension.front.right.spring.motion_ratio))
        print("RL Spring MR: {}".format(self.suspension.rear.left.spring.motion_ratio))
        print("RR Spring MR: {}".format(self.suspension.rear.right.spring.motion_ratio))
        print("")

        # Suspension ARB stiffness
        print("Suspension ARB stiffness:")
        print("Front ARB Stiffness: {}".format(self.suspension.front.ARB.stiffness))
        print("Rear ARB Stiffness: {}".format(self.suspension.rear.ARB.stiffness))
        print("")

        # Suspension ARB motion ratio
        print("Suspension ARB stiffness:")
        print("Front ARB MR: {}".format(self.suspension.front.ARB.motion_ratio))
        print("Rear ARB MR: {}".format(self.suspension.rear.ARB.motion_ratio))
        print("")

        # Roll Center
        print("Roll Center:")
        print("Front Roll Center: {}".format(self.suspension.front.roll_center))
        print("Rear Roll Center: {}".format(self.suspension.rear.roll_center))
        print("")
        print("")



    def pos_process_vehicle(self):
        self.deltaZ = self.calculate_distance_SM_CG_to_roll_axis()
