from Design.BasicProperties_D import BasicProperties_D

#Todo Add a name as an input. Like we have in the design folder
class BasicProperties_S(BasicProperties_D):
    def __init__(self, aBasicProperties_D = None):

        if aBasicProperties_D is None:
            BasicProperties_D.__init__(self, 0, 0, 0, 0)
        else:
            BasicProperties_D.__init__(self,
                                       aBasicProperties_D.weight,
                                       aBasicProperties_D.wheelbase,
                                       aBasicProperties_D.suspended_weight_distribution,
                                       aBasicProperties_D.suspended_weight_CG)






    def set_basic_properties(self, aBasicProperties_D):
        self.weight = aBasicProperties_D.weight
        self.wheelbase = aBasicProperties_D.wheelbase
        self.suspended_weight_distribution = aBasicProperties_D.suspended_weight_distribution
        self.suspended_weight_CG = aBasicProperties_D.suspended_weight_CG
