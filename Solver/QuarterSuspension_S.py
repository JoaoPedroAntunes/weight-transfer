from Design.QuarterSuspension_D import QuarterSuspension_D
from Solver.ARB_S import ARB_S
from Solver.Spring_S import Spring_S
from Solver.Tire_S import Tire_S

#Todo Add a name as an input. Like we have in the design folder
class QuarterSuspension_S():
    def __init__(self, aQuarterSuspension_D : QuarterSuspension_D = None):

        if aQuarterSuspension_D is None:
            self.spring = Spring_S()
            self.tire = Tire_S()
            self.non_suspended_mass = 0
            self.non_suspended_mass_CG = 0

        else:

            self.spring = Spring_S(aQuarterSuspension_D.spring)
            self.tire = Tire_S(aQuarterSuspension_D.tire)
            self.non_suspended_mass = aQuarterSuspension_D.non_suspended_mass
            self.non_suspended_mass_CG = aQuarterSuspension_D.non_suspended_mass_CG
