import math

from sympy import Point3D

from Design.HalfSuspension_D import HalfSuspension_D
from Solver.ARB_S import ARB_S
from Solver.QuarterSuspension_S import QuarterSuspension_S


#Todo Add a name as an input. Like we have in the design folder


class HalfSuspension_S():
    def __init__(self, aHalfSuspension_D: HalfSuspension_D = None):

        if aHalfSuspension_D is None:
            self.left = QuarterSuspension_S()
            self.right = QuarterSuspension_S()
            self.ARB = ARB_S()
            self.roll_center = Point3D(0, 0, 0)
            self.pitch_center = Point3D(0, 0, 0)
            self.non_susp_mass_CG = 0,
            self.track = 0

        else:

            self.left = QuarterSuspension_S(aHalfSuspension_D.left)
            self.right = QuarterSuspension_S(aHalfSuspension_D.right)
            self.ARB = ARB_S(aHalfSuspension_D.ARB)
            self.roll_center = aHalfSuspension_D.roll_center
            self.pitch_center = aHalfSuspension_D.pitch_center
            self.non_susp_mass_CG = aHalfSuspension_D.non_susp_mass_CG
            self.track = aHalfSuspension_D.track

    def calculate_spring_anti_roll_moment(self):
        return (math.pow(self.track, 2) * math.tan(math.radians(1)) * self.left.spring.calculate_wheel_rate()) / 2

    def calculate_ARB_anti_roll_moment(self):
        return (self.ARB.stiffness * math.pow(self.track, 2) * math.tan(math.radians(1))) / math.pow(
            self.ARB.motion_ratio, 2)
