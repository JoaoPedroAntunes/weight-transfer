from Design.Aerodynamics_D import Aerodynamics_D

#Todo Add a name as an input. Like we have in the design folder
class Aerodynamics_S(Aerodynamics_D):
    """
    A very simple GUI application

    This example illustrates writing docstrings for a class.

    :param master: a master Tkinter widget (opt.)

    Example::

        app = Application()
    """
    def __init__(self, aAero_D : Aerodynamics_D() = None, name = ""):

        if aAero_D is None:
            Aerodynamics_D.__init__(self)
        else:
            Aerodynamics_D.__init__(self,
                                    aAero_D.air_density,
                                    aAero_D.downforce_distribution,
                                    aAero_D.frontal_area,
                                    aAero_D.downforce_coefficient,
                                    aAero_D.name)

        self.name = name

    def calculate_total_downforce(self, speed) -> float:
        return 0.5 * self.air_density * self.downforce_coefficient * self.frontal_area * speed * speed

    def calculate_front_downforce(self, speed) -> float:
        return self.calculate_total_downforce(speed) * self.downforce_coefficient

    def calculate_rear_downforce(self, speed) -> float:
        return self.calculate_total_downforce(speed) * (1 - self.downforce_coefficient)
