from Design.Suspension_D import Suspension_D
from Design.HalfSuspension_D import HalfSuspension_D
from Solver.HalfSuspension_S import HalfSuspension_S

#Todo Add a name as an input. Like we have in the design folder
class Suspension_S():
    def __init__(self, aSuspension_D : Suspension_D = None):

        if aSuspension_D is None:
            self.front = HalfSuspension_S()
            self.rear = HalfSuspension_S()
        else:
            self.front = HalfSuspension_S(aSuspension_D.front)
            self.rear = HalfSuspension_S(aSuspension_D.rear)



