import math
from Design.Spring_D import Spring_D

#Todo Add a name as an input. Like we have in the design folder
class Spring_S(Spring_D):
    def __init__(self, aSpring_D : Spring_D = None):
        if aSpring_D is None:
            self.stiffness = 0
            self.motion_ratio = 0
        else:
            Spring_D.__init__(self, aSpring_D.stiffness, aSpring_D.motion_ratio)

    def set_spring(self, aSpring_D: Spring_D):
        self.stiffness = aSpring_D.stiffness
        self.motion_ratio = aSpring_D.motion_ratio

    def calculate_wheel_rate(self):
        return self.stiffness / math.pow(self.motion_ratio, 2)
