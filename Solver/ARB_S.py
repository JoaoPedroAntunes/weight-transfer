from Design.ARB_D import ARB_D

#Todo Add a name as an input. Like we have in the design folder
class ARB_S(ARB_D):
    def __init__(self, aARB_D : ARB_D = None):

        if aARB_D is None:
            ARB_D.__init__(self)
        else:
            ARB_D.__init__(self,
                           aARB_D.stiffness,
                           aARB_D.motion_ratio)



    def set_ARB(self,aARB_D : ARB_D):
        self.stiffness = aARB_D.stiffness
        self.motion_ratio = aARB_D.motion_ratio
