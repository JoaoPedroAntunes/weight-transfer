from Design.Inputs_D import Inputs_D


class Inputs_S(Inputs_D):
    def __init__(self, inputs_D: Inputs_D() = None, name=""):

        if inputs_D is None:
            Inputs_D.__init__(self)
        else:
            Inputs_D.__init__(self,
                              inputs_D.longitudinal_acceleration,
                              inputs_D.lateral_acceleration,
                              inputs_D.longitudinal_velocity)
            self.__set_input_array_to_same_size__()

        self.name = name
        self.steps = []



    def __generate_simulation_steps__(self):
        max_steps = max(len(self.longitudinal_acceleration), len(self.lateral_acceleration), len(self.longitudinal_velocity))

        for i in range(0, max_steps, 1):
            self.steps.append(i)

    def __validated_input__(self) -> bool:
        if not len(self.longitudinal_acceleration) == len(self.lateral_acceleration) == len(self.longitudinal_velocity):
            print("Error [1000] - The inputs size don't match.")
            return False
        else:
            return True

    def __set_input_array_to_same_size__(self):
        max_array_size = max(len(self.longitudinal_acceleration), len(self.lateral_acceleration),
                             len(self.longitudinal_velocity))

        if len(self.longitudinal_velocity) < max_array_size:
            for i in range(len(self.longitudinal_velocity), max_array_size, 1):
                self.longitudinal_velocity.append(self.longitudinal_velocity, 0)

        if len(self.lateral_acceleration) < max_array_size:
            for i in range(len(self.lateral_acceleration), max_array_size, 1):
                self.lateral_acceleration.append(self.lateral_acceleration, 0)

        if len(self.longitudinal_acceleration) < max_array_size:
            for i in range(len(self.longitudinal_acceleration), max_array_size, 1):
                self.longitudinal_acceleration.append(self.longitudinal_acceleration, 0)
