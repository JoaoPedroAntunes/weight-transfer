import copy

import numpy as np

import Globals
from Design.Vehicle_D import Vehicle_D


# TODO understand how to int a Enum
# TODO pass this to a class.
def create_vehicle_batch(parameter,
                         vehicle_D: Vehicle_D,
                         initial_value, final_value, step_size) -> list:
    vehicle_D_list = []
    create_values = np.linspace(initial_value, final_value, step_size)

    #TODO add the remaining fields missing
    for i in range(len(create_values)):
        vehicle_D_deepcopy = copy.deepcopy(vehicle_D)

        if parameter == Globals.VehicleParametersName.air_density.name:
            vehicle_D_deepcopy.aerodynamics.air_density = create_values[i]

        elif parameter == Globals.VehicleParametersName.downforce_distribution.name:
            vehicle_D_deepcopy.aerodynamics.downforce_distribution = create_values[i]

        elif parameter == Globals.VehicleParametersName.frontal_area.name:
            vehicle_D_deepcopy.aerodynamics.frontal_area = create_values[i]

        elif parameter == Globals.VehicleParametersName.downforce_coefficient.name:
            vehicle_D_deepcopy.aerodynamics.downforce_coefficient = create_values[i]

        elif parameter == Globals.VehicleParametersName.total_weight.name:
            vehicle_D_deepcopy.basic_properties.mass = create_values[i]

        elif parameter == Globals.VehicleParametersName.wheelbase.name:
            vehicle_D_deepcopy.basic_properties.wheelbase = create_values[i]

        elif parameter == Globals.VehicleParametersName.front_track.name:
            vehicle_D_deepcopy.basic_properties.front_track = create_values[i]

        elif parameter == Globals.VehicleParametersName.rear_track.name:
            vehicle_D_deepcopy.basic_properties.rear_track = create_values[i]

        elif parameter == Globals.VehicleParametersName.weight_distribution.name:
            vehicle_D_deepcopy.basic_properties.mass_distribution = create_values[i]

        elif parameter == Globals.VehicleParametersName.total_weight_CG_height.name:
            vehicle_D_deepcopy.basic_properties.mass_CG_height = create_values[i]

        elif parameter == Globals.VehicleParametersName.front_ARB_stiffness.name:
            vehicle_D_deepcopy.suspension.front.ARB.stiffness = create_values[i]

        elif parameter == Globals.VehicleParametersName.rear_ARB_stiffness.name:
            vehicle_D_deepcopy.suspension.rear.ARB.stiffness = create_values[i]

        elif parameter == Globals.VehicleParametersName.front_ARB_motion_ratio.name:
            vehicle_D_deepcopy.suspension.front.ARB.motion_ratio = create_values[i]

        elif parameter == Globals.VehicleParametersName.rear_ARB_motion_ratio.name:
            vehicle_D_deepcopy.suspension.front.ARB.motion_ratio = create_values[i]

        elif parameter == Globals.VehicleParametersName.spring_stiffness_FL.name:
            vehicle_D_deepcopy.suspension.front.left.spring.stiffness = create_values[i]

        elif parameter == Globals.VehicleParametersName.spring_stiffness_FR.name:
            vehicle_D_deepcopy.suspension.front.right.spring.stiffness = create_values[i]

        elif parameter == Globals.VehicleParametersName.spring_stiffness_RL.name:
            vehicle_D_deepcopy.suspension.rear.left.spring.stiffness = create_values[i]

        elif parameter == Globals.VehicleParametersName.spring_stiffness_RR.name:
            vehicle_D_deepcopy.suspension.rear.right.spring.stiffness = create_values[i]

        else:
            Exception("Non parameter was selected")

        vehicle_D_list.append(vehicle_D_deepcopy)

    return vehicle_D_list
