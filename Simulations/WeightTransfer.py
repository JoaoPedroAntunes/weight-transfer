import math


def calculate_non_suspended_mass_weight_transfer(non_susp_mass, lat_acc, non_susp_mass_CG, track):
    return (non_susp_mass * lat_acc * non_susp_mass_CG / track) / 9.81


def calculate_geometry_weight_transfer(susp_mass, susp_mass_weight_distribution, lat_acc, roll_center, track):
    return (susp_mass * susp_mass_weight_distribution * lat_acc * roll_center / track) / 9.81


def calculate_elastic_weight_transfer(susp_mass, lat_acc, deltaZ, AR_spring, AR_ARB, AR_total, track):
    return ((susp_mass * lat_acc * deltaZ * (AR_spring + AR_ARB) / AR_total) / track) / 9.81


def calculate_spring_anti_roll_moment(track, wheel_rate):
    return (math.pow(track, 2) * math.tan(math.radians(1)) * wheel_rate) / 2


def calculate_ARB_anti_roll_moment(ARB_stiffness, track, ARB_MR):
    return (ARB_stiffness * math.pow(track, 2) * math.tan(math.radians(1))) / math.pow(ARB_MR, 2)


def calculate_dynamic_load(total_mass,
                           weight_distribution,
                           downforce,
                           downforce_distribution,
                           total_weight_transfer):
    return


def calculate_weight_transfer(self):
    pass


def calculate_roll_moment(self):
    pass


def calculate_roll_angle(self):
    pass


def calculate_roll_gradient(self):
    pass
