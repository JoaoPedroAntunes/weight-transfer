from Design.Spring_D import Spring_D
from Design.Tire_D import Tire_D
from Managers.ProjectFramework import ProjectFramework


class QuarterSuspension_D(ProjectFramework):
    """

    Creates an instance of the QuarterSuspension_D class

    :param spring_D: the Spring_D class
    :param tire_D: the tire_D class
    :param non_suspended_mass: the non suspended weight
    :param non_suspended_mass_CG: the non suspended weight cg
    :param name: Name of the object [str]

    Example::

        obj = QuarterSuspension_D()
    """
    def __init__(self,
                 spring_D : Spring_D() = None,
                 tire_D : Tire_D() = None,
                 non_suspended_mass = 0,
                 non_suspended_mass_CG = 0,
                 name = ""):

        ProjectFramework.__init__(self, name, ".qsus", "0.0.0")

        # When creating the object if no argument for any of the classes was passed then create an empty
        # object
        if spring_D is None:
            self.spring = Spring_D()
        else:
            self.spring = spring_D

        if tire_D is None:
            self.tire = Tire_D()
        else:
            self.tire = tire_D

        self.non_suspended_mass = non_suspended_mass
        self.non_suspended_mass_CG = non_suspended_mass_CG

    def init_default_values(self):
        """
        Initializes default values for the class

        :return: Nothing
        """
        self.spring.init_default_values()
        self.tire.init_default_values()
        self.non_suspended_mass = 38 * 9.81
        self.non_susp_mass_CG = 0.330

    # region save/load

    # Save object
    def save_object(self, file_path=""):
        """
        Saves this object into a JSON file into the directory specified in file_path

        :param file_path: file_path where the Json file is to be saved. Ex: "Desktop/User/" the name and extension are
        automatically added from the object properties self.name self.extension
        :return: Nothing

        .. warning::

            To save an object the self.name properties cannot be empty.

        Example::

            obj = QuarterSuspension_D() #Create Class
            obj.init_default_values() #Init default values
            obj.name = "Example"  #A name for the object is necessary if you want to save it
            obj.save_object("C:\\Users\\joaop\\Desktop") #Save

        """

        # The local variable data, is a dictionary where we will be saving all the parameters that we want. Typically
        # this parameters will be the name, version, date, user (from the project Framework class), and the additional
        # parameters that are part of the class itself. After that the function will call the save_to_json function which
        # will save the data into a file.

        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
            "non_suspended_mass" : self.non_suspended_mass,
            "non_suspended_mass_CG" : self.non_suspended_mass_CG
        }
        self.save_to_json(file_path, data)

    # Load Object
    def load_object(self, file_path):
        """
        Loads the values from a JSON file to the object

        :param file: name of the file with extension Ex: filename.ext
        :return: None

        In the following example we are going to load an object from the aerodynamics class.
        In this case it is necessary to first create the object and then issue the load_object function to load the
        values into the class

        Example::

            obj = QuarterSuspension_D() #Create Class
            obj.load_object("C:\\Users\\joaop\\Desktop\\Example.qsus") #Load

        """

        data = self.load_from_json(file_path)

        # Get the parameters from the json file.
        self.name = data.get("name")
        self.version = data.get("version")
        self.date = data.get("date")
        self.user = data.get("user")
        self.non_suspended_mass = data.get("non_suspended_mass")
        self.non_suspended_mass_CG = data.get("non_suspended_mass_CG")

    # endregion
