import numpy as np

from Managers.ProjectFramework import ProjectFramework


class Inputs_D(ProjectFramework):
    """

    Creates an instance of the Aerodynamics_D class

    :param longitudinal_acceleration: An array of longitudinal acceleration values
    :param lateral_acceleration: An array of lateral acceleration values
    :param longitudinal_velocity: An array of longitudinal velocity values
    :param name: Name of the object [str]

    Example::

        obj = Aerodynamics_D()
    """
    def __init__(self,
                 longitudinal_acceleration: np.array = None,
                 lateral_acceleration: np.array = None,
                 longitudinal_velocity: np.array = None,
                 name = ""):

        ProjectFramework.__init__(self, name, ".inp", "0.0.0")

        # When creating the object if no argument for any of the classes was passed then create an empty
        # object
        if longitudinal_acceleration is None:
            self.longitudinal_acceleration = np.array([0])
        else:
            self.longitudinal_acceleration = longitudinal_acceleration

        if lateral_acceleration is None:
            self.lateral_acceleration = np.array([0])
        else:
            self.lateral_acceleration = lateral_acceleration

        if longitudinal_velocity is None:
            self.longitudinal_velocity = np.array([0])
        else:
            self.longitudinal_velocity = longitudinal_velocity

    def init_default_values(self):
        """
        Initializes default values for the class

        :return: Nothing
        """
        self.longitudinal_acceleration = np.array(0)
        self.lateral_acceleration = np.array(9.81)
        self.longitudinal_velocity = np.array(10)

    # region save/load

    # Save object
    def save_object(self, file_path=""):
        """
        Saves this object into a JSON file into the directory specified in file_path

        :param file_path: file_path where the Json file is to be saved. Ex: "Desktop/User/" the name and extension are
        automatically added from the object properties self.name self.extension
        :return: Nothing

        .. warning::

            To save an object the self.name properties cannot be empty.

        Example::

            obj = Aerodynamics_D() #Create Class
            obj.init_default_values() #Init default values
            obj.name = "Example"  #A name for the object is necessary if you want to save it
            obj.save_object("C:\\Users\\joaop\\Desktop") #Save

        """

        # The local variable data, is a dictionary where we will be saving all the parameters that we want. Typically
        # this parameters will be the name, version, date, user (from the project Framework class), and the additional
        # parameters that are part of the class itself. After that the function will call the save_to_json function which
        # will save the data into a file.

        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
            "longitudinal_acceleration": self.longitudinal_acceleration.tolist(),
            "lateral_acceleration": self.lateral_acceleration.tolist(),
            "longitudinal_velocity": self.longitudinal_velocity.tolist(),
        }

        self.save_to_json(file_path, data)

    # Load Object
    def load_object(self, file_path):
        """
        Loads the values from a JSON file to the object

        :param file: name of the file with extension Ex: filename.ext
        :return: None

        In the following example we are going to load an object from the aerodynamics class.
        In this case it is necessary to first create the object and then issue the load_object function to load the
        values into the class

        Example::

            obj = Inputs_D() #Create Class
            obj.load_object("C:\\Users\\joaop\\Desktop\\Example.inp") #Load

        """
        data = self.load_from_json(file_path)

        self.name = data.get("name")
        self.version = data.get("version")
        self.date = data.get("date")
        self.user = data.get("user")
        self.longitudinal_acceleration = np.asarray(data.get("longitudinal_acceleration"))
        self.lateral_acceleration = np.asarray(data.get("lateral_acceleration"))
        self.longitudinal_velocity = np.asarray(data.get("longitudinal_velocity"))

    # endregion

    def create_linear_lateral_acceleration_input(self, initial_value, final_value, step_size):
        """
        Using the np.linspace function create an array with an equal step

        :param initial_value: The initial value of the array. This value will be on the first row of the array
        :param final_value: The final value of the array. This value will be on the last row of the array
        :param step_size: the step of the array
        :return: None

        Example::

            obj = Inputs_D() #Create Class
            obj.create_linear_lateral_acceleration_input(0,10,1) # Create an array of lateral acceleration from 0 to 10m/s2
            resultant array: [0, 1, 2, 3, 4, 5, 6, 8, 9, 10]

        """
        self.lateral_acceleration = np.linspace(initial_value, final_value, step_size)

    def create_linear_longitudinal_acceleration_input(self, initial_value, final_value, step_size):
        """
        Using the np.linspace function create an array with an equal step

        :param initial_value: The initial value of the array. This value will be on the first row of the array
        :param final_value: The final value of the array. This value will be on the last row of the array
        :param step_size: the step of the array
        :return: None

        Example::

            obj = Inputs_D() #Create Class
            obj.create_linear_longitudinal_acceleration_input(0,10,1) # Create an array of lateral acceleration from 0 to 10m/s2
            resultant array: [0, 1, 2, 3, 4, 5, 6, 8, 9, 10]

        """
        self.longitudinal_acceleration = np.linspace(initial_value, final_value, step_size)

    def create_linear_longitudinal_velocity_input(self, initial_value, final_value, step_size):
        """
        Using the np.linspace function create an array with an equal step

        :param initial_value: The initial value of the array. This value will be on the first row of the array
        :param final_value: The final value of the array. This value will be on the last row of the array
        :param step_size: the step of the array
        :return: None

        Example::

            obj = Inputs_D() #Create Class
            obj.create_linear_longitudinal_velocity_input(0,10,1) # Create an array of lateral acceleration from 0 to 10m/s2
            resultant array: [0, 1, 2, 3, 4, 5, 6, 8, 9, 10]

        """
        self.longitudinal_velocity = np.linspace(initial_value, final_value, step_size)
