from sympy import Point3D

from Design.Aerodynamics_D import Aerodynamics_D
from Design.BasicProperties_D import BasicProperties_D
from Design.Suspension_D import Suspension_D
from Managers.ProjectFramework import ProjectFramework, DesignDirectory


class Vehicle_D(ProjectFramework):
    """

    Creates an instance of the Vehicle_D class

    :param basic_properties_D: The basic properties class
    :param suspension_D: suspension class
    :param aerodynamics_D: aerodynamics
    :param name: Name of the object [str]

    Example::

        obj = Aerodynamics_D()
    """
    def __init__(self,
                 basic_properties_D: BasicProperties_D = None,
                 suspension_D: Suspension_D = None,
                 aerodynamics_D: Aerodynamics_D = None,
                 name = ""):

        ProjectFramework.__init__(self, name, ".veh", "0.0.0")

        # When creating the object if no argument for any of the classes was passed then create an empty
        # object
        if basic_properties_D is None:
            self.basic_properties = BasicProperties_D()
        else:
            self.basic_properties = basic_properties_D

        if aerodynamics_D is None:
            self.aerodynamics = Aerodynamics_D()
        else:
            self.aerodynamics = basic_properties_D

        if suspension_D is None:
            self.suspension = Suspension_D()
        else:
            self.suspension = suspension_D


    def init_default_values(self):
        """
        Initializes default values for the class

        :return: Nothing
        """
        # Aerodynamics
        self.aerodynamics.name = "Default aero"
        self.aerodynamics.downforce_coefficient = 2.2
        self.aerodynamics.frontal_area = 1.75
        self.aerodynamics.air_density = 1.23
        self.aerodynamics.downforce_distribution = 0.46

        # Basic Properties
        self.basic_properties.name = "Default basic properties"
        self.basic_properties.weight = 994 * 9.81
        self.basic_properties.suspended_weight_CG = 0.27
        self.basic_properties.suspended_weight_distribution = 0.47
        self.basic_properties.wheelbase = 2.980

        # Suspension Track
        self.suspension.name = "Default suspension"
        self.suspension.front.track = 1.640
        self.suspension.rear.track = 1.592

        # Suspension spring stiffness
        self.suspension.front.left.spring.name = "Default front left spring"
        self.suspension.front.right.spring.name = "Default front right spring"
        self.suspension.rear.left.spring.name = "Default rear left spring"
        self.suspension.rear.right.spring.name = "Default rear right spring"
        self.suspension.front.left.spring.stiffness = 127.1*1000
        self.suspension.front.right.spring.stiffness = 127.1*1000
        self.suspension.rear.left.spring.stiffness = 103.4*1000
        self.suspension.rear.right.spring.stiffness = 103.4*1000

        #Suspension spring motion ratio
        self.suspension.front.left.spring.motion_ratio = 0.82
        self.suspension.front.right.spring.motion_ratio = 0.82
        self.suspension.rear.left.spring.motion_ratio = 0.95
        self.suspension.rear.right.spring.motion_ratio = 0.95

        #Suspension ARB stiffness
        self.suspension.front.ARB.name = "Default front ARB"
        self.suspension.front.ARB.stiffness = 180*1000
        self.suspension.rear.ARB.stiffness = 25.4*1000

        #Suspension ARB motion ratio
        self.suspension.rear.ARB.name = "Default rear ARB"
        self.suspension.front.ARB.motion_ratio = 1.15
        self.suspension.rear.ARB.motion_ratio = 1.03

        #Roll Center
        self.suspension.front.name = "Default front half"
        self.suspension.rear.name = "Default rear half"
        self.suspension.front.roll_center = Point3D(0, 0, 0.05)
        self.suspension.rear.roll_center = Point3D(0, 0, 0.08)

        #Non Suspended Mass
        self.suspension.front.left.non_suspended_mass = 38*9.81
        self.suspension.front.right.non_suspended_mass = 38*9.81
        self.suspension.rear.left.non_suspended_mass = 42*9.81
        self.suspension.rear.right.non_suspended_mass = 42*9.81

        self.suspension.front.non_susp_mass_CG = 0.330
        self.suspension.rear.non_susp_mass_CG = 0.355

        # Tire
        self.suspension.front.left.tire.name = "Default front left tire"
        self.suspension.front.right.tire.name = "Default front right tire"
        self.suspension.rear.left.tire.name = "Default rear left tire"
        self.suspension.rear.right.tire.name = "Default rear right tire"

    def print_vehicle_properties(self):
        """
        Prints the current values loaded on the vehicle_D class
        """
        print("Aerodynamics:")
        print("Downforce Coefficient: {}".format(self.aerodynamics.downforce_coefficient))
        print("Frontal Area: {}".format(self.aerodynamics.frontal_area))
        print("Air Density: {}".format(self.aerodynamics.air_density))
        print("Downforce Distribution: {}".format(self.aerodynamics.downforce_distribution))
        print("")
        
        print("Basic Properties")
        print("Mass: {}".format(self.basic_properties.weight))
        print("Mass CG Height: {}".format(self.basic_properties.suspended_weight_CG))
        print("Mass Distribution: {}".format(self.basic_properties.suspended_weight_distribution))
        print("Front Track: {}".format(self.suspension.front.track))
        print("Rear Track: {}".format(self.suspension.rear.track))
        print("")

        print("Suspension spring stiffness:")
        print("FL Spring Stiffness: {}".format(self.suspension.front.left.spring.stiffness))
        print("FR Spring Stiffness: {}".format(self.suspension.front.right.spring.stiffness))
        print("RL Spring Stiffness: {}".format(self.suspension.rear.left.spring.stiffness))
        print("RR Spring Stiffness: {}".format(self.suspension.rear.right.spring.stiffness))
        print("")

        print("Suspension spring MR:")
        print("FL Spring MR: {}".format(self.suspension.front.left.spring.motion_ratio))
        print("FR Spring MR: {}".format(self.suspension.front.right.spring.motion_ratio))
        print("RL Spring MR: {}".format(self.suspension.rear.left.spring.motion_ratio))
        print("RR Spring MR: {}".format(self.suspension.rear.right.spring.motion_ratio))
        print("")

        print("Suspension ARB stiffness:")
        print("Front ARB Stiffness: {}".format(self.suspension.front.ARB.stiffness))
        print("Rear ARB Stiffness: {}".format(self.suspension.rear.ARB.stiffness))
        print("")

        print("Suspension ARB stiffness:")
        print("Front ARB MR: {}".format(self.suspension.front.ARB.motion_ratio))
        print("Rear ARB MR: {}".format(self.suspension.rear.ARB.motion_ratio))
        print("")

        print("Roll Center:")
        print("Front Roll Center: {}".format(self.suspension.front.roll_center))
        print("Rear Roll Center: {}".format(self.suspension.rear.roll_center))
        print("")
        print("")

    # region save/load

    # Save object
    def save_object(self, file_path):
        """
              Saves this object into a JSON file into the directory specified in file_path

              :param file_path: file_path where the Json file is to be saved. Ex: "Desktop/User/" the name and extension are
              automatically added from the object properties self.name self.extension
              :return: Nothing

              .. warning::

                  To save an object the self.name properties cannot be empty.

              Example::

                  obj = Vehicle_D() #Create Class
                  obj.init_default_values() #Init default values
                  obj.name = "Example"  #A name for the object is necessary if you want to save it
                  obj.save_object("C:\\Users\\joaop\\Desktop") #Save

              """

        # The local variable data, is a dictionary where we will be saving all the parameters that we want. Typically
        # this parameters will be the name, version, date, user (from the project Framework class), and the additional
        # parameters that are part of the class itself. After that the function will call the save_to_json function which
        # will save the data into a file.

        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
            "basic_properties_name" : self.basic_properties.name,
            "aerodynamics_name": self.aerodynamics.name,
            "suspension_front_name": self.suspension.front.name,
            "suspension_rear_name": self.suspension.rear.name,
            "ARB_front_name": self.suspension.front.ARB.name,
            "ARB_rear_name": self.suspension.rear.ARB.name,
            "spring_front_left_name": self.suspension.front.left.spring.name,
            "spring_front_right_name": self.suspension.front.right.spring.name,
            "spring_rear_left_name": self.suspension.rear.left.spring.name,
            "spring_rear_right_name": self.suspension.rear.right.spring.name,
            "tire_front_left_name": self.suspension.front.left.tire.name,
            "tire_front_right_name": self.suspension.front.right.tire.name,
            "tire_rear_left_name": self.suspension.rear.left.tire.name,
            "tire_rear_right_name": self.suspension.rear.right.tire.name
        }

        self.save_to_json(file_path, data)

    # Load Object
    def load_object(self, file_path):
        """
        Loads the values from a JSON file to the object

        :param file: name of the file with extension Ex: filename.ext
        :return: None

        In the following example we are going to load an object from the aerodynamics class.
        In this case it is necessary to first create the object and then issue the load_object function to load the
        values into the class

        Example::

            obj = Vehicle_D() #Create Class
            obj.load_object("C:\\Users\\joaop\\Desktop\\Example.veh") #Load

        """
        data = self.load_from_json(file_path)

        self.basic_properties.name = data.get("basic_properties_name")
        self.aerodynamics.name = data.get("aerodynamics_name")
        self.suspension.front.name = data.get("suspension_front_name")
        self.suspension.rear.name = data.get("suspension_rear_name")
        self.suspension.front.ARB.name = data.get("ARB_front_name")
        self.suspension.rear.ARB.name = data.get("ARB_rear_name")
        self.suspension.front.left.spring.name = data.get("spring_front_left_name")
        self.suspension.front.right.spring.name = data.get("spring_front_right_name")
        self.suspension.rear.left.spring.name = data.get("spring_rear_left_name")
        self.suspension.rear.right.spring.name = data.get("spring_rear_right_name")
        self.suspension.front.left.tire.name = data.get("tire_front_left_name")
        self.suspension.front.right.tire.name = data.get("tire_front_right_name")
        self.suspension.rear.left.tire.name = data.get("tire_rear_left_name")
        self.suspension.rear.right.tire.name = data.get("tire_rear_right_name")

        # Remove the last item so that we can get the project directory
        file_path_split = str(file_path).split("\\")
        project_directory = ""
        for i in range(0, len(file_path_split) - 2):
            if i == 0:
                project_directory = file_path_split[i]
            else:
                project_directory = project_directory + "\\" + file_path_split[i]

        #TODO test if this is working
        if not self.basic_properties.name == "":
            self.basic_properties.load_object(
                project_directory + DesignDirectory.basic_properties.value + "\\" + self.basic_properties.name + self.basic_properties.extension)

        if not self.aerodynamics.name == "":
            self.aerodynamics.load_object(
                project_directory + DesignDirectory.aerodynamics.value + "\\" + self.aerodynamics.name + self.aerodynamics.extension)
        # Suspension
        if not self.suspension.front.name == "":
            self.suspension.front.load_object(
                project_directory + DesignDirectory.suspension.value + "\\" + self.suspension.front.name + self.suspension.front.extension)

        if not self.suspension.rear.name == "":
            self.suspension.rear.load_object(
                project_directory + DesignDirectory.suspension.value + "\\" + self.suspension.rear.name + self.suspension.rear.extension)

        # ARB
        if not self.suspension.front.ARB.name == "":
            self.suspension.front.ARB.load_object(
                project_directory + DesignDirectory.ARB.value + "\\" + self.suspension.front.ARB.name + self.suspension.front.ARB.extension)

        if not self.suspension.rear.ARB.name == "":
            self.suspension.rear.ARB.load_object(
                project_directory + DesignDirectory.ARB.value + "\\" + self.suspension.rear.ARB.name + self.suspension.rear.ARB.extension)

        # Springs
        if not self.suspension.front.left.spring.name == "":
            self.suspension.front.left.spring.load_object(
                project_directory + DesignDirectory.Springs.value + "\\" + self.suspension.front.left.spring.name + self.suspension.front.left.spring.extension)

        if not self.suspension.front.right.spring.name == "":
            self.suspension.front.right.spring.load_object(
                project_directory + DesignDirectory.Springs.value + "\\" + self.suspension.front.right.spring.name + self.suspension.front.right.spring.extension)

        if not self.suspension.rear.left.spring.name == "":
            self.suspension.rear.left.spring.load_object(
                project_directory + DesignDirectory.Springs.value + "\\" + self.suspension.rear.left.spring.name + self.suspension.rear.left.spring.extension)

        if not self.suspension.rear.right.spring.name == "":
            self.suspension.rear.right.spring.load_object(
                project_directory + DesignDirectory.Springs.value + "\\" + self.suspension.rear.right.spring.name + self.suspension.rear.right.spring.extension)

        # Tires
        if not self.suspension.front.left.tire.name == "":
            self.suspension.front.left.tire.load_object(
                project_directory + DesignDirectory.tires.value + "\\" + self.suspension.front.left.tire.name + self.suspension.front.left.tire.extension)

        if not self.suspension.front.right.tire.name == "":
            self.suspension.front.right.tire.load_object(
                project_directory + DesignDirectory.tires.value + "\\" + self.suspension.front.right.tire.name + self.suspension.front.right.tire.extension)

        if not self.suspension.rear.left.tire.name == "":
            self.suspension.rear.left.tire.load_object(
                project_directory + DesignDirectory.tires.value + "\\" + self.suspension.rear.left.tire.name + self.suspension.rear.left.tire.extension)

        if not self.suspension.rear.right.tire.name == "":
            self.suspension.rear.right.tire.load_object(
                project_directory + DesignDirectory.tires.value + "\\" + self.suspension.rear.right.tire.name + self.suspension.rear.right.tire.extension)

    # endregion
