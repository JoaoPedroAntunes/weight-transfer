from Design.HalfSuspension_D import HalfSuspension_D


class Suspension_D():
    """

    Creates an instance of the Suspension_D class. This class is different from the others
    in the sense that it doesn't have a load or save object. The main reason is that this class is
    used so that 2 classes of the HalfSuspension_D class can be used. This just makes the code
    easier to use because then I can create a front_D and a rear_D HalfSuspension_D class class.

    :param front_suspension_D: the front suspension
    :param rear_suspension_D: the rear suspension
    :param name: Name of the object [str]

    Example::

        obj = Suspension_D()
    """
    def __init__(self,
                 front_suspension_D: HalfSuspension_D = None,
                 rear_suspension_D: HalfSuspension_D = None,
                 name = ""):

        self.name = name
        self.front_D : HalfSuspension_D()
        self.rear_D : HalfSuspension_D()

        # When creating the object if no argument for any of the classes was passed then create an empty
        # object
        if front_suspension_D is None:
            self.front = HalfSuspension_D()
        else:
            self.front = front_suspension_D

        if rear_suspension_D is None:
            self.rear = HalfSuspension_D()
        else:
            self.rear = rear_suspension_D


    def init_default_values(self):
        """
        Initializes default values for the class. In this case not that tit will create a whole suspension
        because when we init the values it will also create the underlying objects.

        :return: Nothing
        """
        self.front.init_default_values()
        self.rear.init_default_values()
