HalfSuspension_D
====================

Contains the properties of a suspension


.. autofunction:: HalfSuspension_D.HalfSuspension_D
.. autofunction:: HalfSuspension_D.HalfSuspension_D.init_default_values
.. autofunction:: HalfSuspension_D.HalfSuspension_D.save_object
.. autofunction:: HalfSuspension_D.HalfSuspension_D.load_object

