BasicProperties_D
====================

Contains the base properties of a vehicle


.. autofunction:: BasicProperties_D.BasicProperties_D
.. autofunction:: BasicProperties_D.BasicProperties_D.init_default_values
.. autofunction:: BasicProperties_D.BasicProperties_D.save_object
.. autofunction:: BasicProperties_D.BasicProperties_D.load_object

