Spring_D
====================

Contains the properties to create spring


.. autofunction:: Spring_D.Spring_D
.. autofunction:: Spring_D.Spring_D.init_default_values
.. autofunction:: Spring_D.Spring_D.save_object
.. autofunction:: Spring_D.Spring_D.load_object