Design
====================
The design package documentation

=================
Introduction
=================
The design package is a class of functions that are responsible for creating the objects that
interact with the user. All this classes have the following functions in common:
* init_default_values
* save_object
* load_object
* clone

=================
Nomenclature
=================
This class is created using the following name standard: [name]_D
A design class is easily identified by the "_D" suffix. This is intended so that when coding
we can easily work with the name directly instead of adding suffix or prefix when create a variable
Example: aerodynamics = Aerodynamics_D() is preferred opposed to aerodynamics_D = Aerodynamics()

=================
Intended use
=================
This classes are to be used as the classes that save and load the project. The user can create
multiple files with this classes. This classes are to be used when you create a form and
pass the user input to the class.
The class will then pass this input to the solver class to perform simulations.

=================
Classes
=================
Below you will find a list of the available classes.

.. toctree::
   :maxdepth: 2

   Aerodynamics_D
   ARB_D
   BasicProperties_D
   HalfSuspension_D
   Inputs_D
   QuarterSuspension_D
   Spring_D
   Suspension_D
   Tire_D
   Vehicle_D
