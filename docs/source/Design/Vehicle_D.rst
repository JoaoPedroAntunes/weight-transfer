Vehicle_D
====================

Contains the properties to create a vehicle

.. autofunction:: Vehicle_D.Vehicle_D
.. autofunction:: Vehicle_D.Vehicle_D.init_default_values
.. autofunction:: Vehicle_D.Vehicle_D.print_vehicle_properties
.. autofunction:: Vehicle_D.Vehicle_D.save_object
.. autofunction:: Vehicle_D.Vehicle_D.load_object