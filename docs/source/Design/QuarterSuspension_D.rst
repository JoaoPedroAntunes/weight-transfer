QuarterSuspension_D
====================

Contains the properties to create a quarter of the suspension


.. autofunction:: QuarterSuspension_D.QuarterSuspension_D
.. autofunction:: QuarterSuspension_D.QuarterSuspension_D.init_default_values
.. autofunction:: QuarterSuspension_D.QuarterSuspension_D.save_object
.. autofunction:: QuarterSuspension_D.QuarterSuspension_D.load_object