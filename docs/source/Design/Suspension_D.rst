Suspension_D
====================

Contains the properties to create a suspension


.. autofunction:: Suspension_D.Suspension_D
.. autofunction:: Suspension_D.Suspension_D.init_default_values