Aerodynamics_D
====================
Contains the properties for aerodynamics


.. autofunction:: Aerodynamics_D.Aerodynamics_D
.. autofunction:: Aerodynamics_D.Aerodynamics_D.init_default_values
.. autofunction:: Aerodynamics_D.Aerodynamics_D.save_object
.. autofunction:: Aerodynamics_D.Aerodynamics_D.load_object