Tire_D
====================

Contains the properties to create a tire

.. autofunction:: Tire_D.Tire_D
.. autofunction:: Tire_D.Tire_D.init_default_values
.. autofunction:: Tire_D.Tire_D.save_object
.. autofunction:: Tire_D.Tire_D.load_object