Inputs_D
====================

Contains the properties to create an input to the model


.. autofunction:: Inputs_D.Inputs_D
.. autofunction:: Inputs_D.Inputs_D.init_default_values
.. autofunction:: Inputs_D.Inputs_D.save_object
.. autofunction:: Inputs_D.Inputs_D.load_object
