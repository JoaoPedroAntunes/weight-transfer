Aerodynamics_W
====================
Controls the interaction between the user and the class Aerodynamics_D

.. autofunction:: Aerodynamics_W
|
.. autofunction:: Aerodynamics_W.on_pushButton_OK_clicked
|