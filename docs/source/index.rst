.. Weight Transfer documentation master file, created by
sphinx-quickstart on Sat Aug 31 10:52:27 2019.
You can adapt this file completely to your liking, but it should at least
contain the root `toctree` directive.

Welcome to Weight Transfer's documentation!
===========================================
Everything you need to know about the WeightTransfer project

=================
Introduction
=================
This documentation serves has the basis for understanding how this software works. You should
start by reading the introduction to have a feel of how the documentation is organized and the
mental model of how the different packages were created.

* Design - Package that contains all the design classes.
* Solver - Package that contains all the solver classes.
* Globals - Global variables used through the project.
* FileManager - Manages all the project files.
* Simulations - Contains all the possible simulations.
* Tests - Unittests
* UserInterface - Contains all the user interface files
** Design - Contains the user interface code for the Design classes
** Simulation - Contains the user interface for the Simulation
** UI - Contains the code generated from the QTcreator tool

.. toctree::
   :maxdepth: 1

   Design/Design
   Solver/Solver
   FileManager/FileManager
   Globals/Globals
   Solver/Solver
   UserInterface/UserInterface
   Simulations/Simulations



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
