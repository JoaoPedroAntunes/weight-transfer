import os

from PyQt5 import QtCore
from PyQt5.QtGui import QFont
from PyQt5.QtWidgets import QTreeWidget, QTreeWidgetItem

from Globals import Globals
from Managers.ProjectFramework import ProjectFramework


class TreeResultsManager(QTreeWidget):
    def __init__(self, tab):
        super().__init__(tab)
        self.project_directory = ""

    def create_master_tree_nodes(self, parent_node_names: Globals.TreeViewResults):
        for i, results_dic in enumerate(parent_node_names):
            top_level_item = QTreeWidgetItem(self)
            top_level_item.setText(0, results_dic.value)

    def load_child_nodes(self, project_directory, parent_node_names: Globals.TreeViewNames):
        for i, results_dic in enumerate(parent_node_names):
            obj_in_folder = os.listdir(project_directory + "\\" + results_dic.value)

            for name in obj_in_folder:
                sub_level_item = QTreeWidgetItem(
                    self.topLevelItem(self.__get_parent_index_from_name__(results_dic.value)))
                obj_loaded = ProjectFramework("", "", "")
                obj_loaded.load_from_json(project_directory + "\\" + results_dic.value + "\\" + name)
                sub_level_item.setText(0, obj_loaded.name)
                self.__set_checkboxes__(sub_level_item)

    def add_obj_to_tree(self, obj_name, parent_node_name):
        index = self.__get_parent_index_from_name__(parent_node_name)

        # Add this new node to the treeview
        sub_level_item = QTreeWidgetItem(self.topLevelItem(index))
        sub_level_item.setText(0, obj_name)
        self.__set_checkboxes__(sub_level_item)

    def __get_parent_index_from_name__(self, parent_node_name) -> int:
        for i in range(self.topLevelItemCount()):

            if self.topLevelItem(i).text(0) == parent_node_name:
                return i

        raise Exception("Couldn't find the parent node")

    def __set_checkboxes__(self, sub_level_item: QTreeWidgetItem):
        sub_level_item.setFlags(sub_level_item.flags() | QtCore.Qt.ItemIsTristate | QtCore.Qt.ItemIsUserCheckable)
        sub_level_item.setCheckState(0, QtCore.Qt.Unchecked)

    def on_itemDoubleClicked(self, item):
        if (item.parent()):
            if (item.checkState(0) == QtCore.Qt.Checked):
                item.setCheckState(0, QtCore.Qt.Unchecked)
                item.setFont(0, QFont("Calibri", weight=QFont.Normal))
            else:
                item.setCheckState(0, QtCore.Qt.Checked)
                item.setFont(0, QFont("Calibri", weight=QFont.Bold))
