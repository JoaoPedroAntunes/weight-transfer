import os

from PyQt5 import QtCore, QtWidgets
from PyQt5.QtWidgets import QTreeWidget, QMessageBox, QTreeWidgetItem

from Design.ARB_D import ARB_D
from Design.Aerodynamics_D import Aerodynamics_D
from Design.BasicProperties_D import BasicProperties_D
from Design.HalfSuspension_D import HalfSuspension_D
from Design.Inputs_D import Inputs_D
from Design.Spring_D import Spring_D
from Design.Tire_D import Tire_D
from Design.Vehicle_D import Vehicle_D
from Globals import Globals
from Managers.ProjectFramework import ProjectFramework
from UserInterface.Design import Aerodynamics_W, BasicProperties_W, Inputs_W, Spring_W, ARB_W, Tire_W, Vehicle_W
from UserInterface.Design.HalfSuspension_W import HalfSuspension_W


class TreeWidgetManager(QTreeWidget):
    def __init__(self, tab):
        super().__init__(tab)
        self.project_directory = ""


    def is_name_already_exists(self, parent_node_name, name) -> bool:
        # Get parent index based on the name
        index = self.__get_parent_index_from_name__(parent_node_name)

        # Loop inside the child nodes to see if the name alreayd exists
        for i in range(self.topLevelItem(index).childCount()):
            if self.topLevelItem(index).child(i).text(0) == name:
                return True

        return False

    def msg_box_name_already_exists(self):
        msg_box = QMessageBox()
        msg_box.setIcon(QMessageBox.Question)
        msg_box.setText("The name already exists")
        msg_box.setInformativeText("Do you want to override it?")
        msg_box.setStandardButtons(QMessageBox.Save | QMessageBox.Discard | QMessageBox.Cancel)
        msg_box.setDefaultButton(QMessageBox.Save)
        ret = msg_box.exec_()
        return ret

    def add_obj_to_tree(self, obj_name, parent_node_name):
        index = self.__get_parent_index_from_name__(parent_node_name)

        # Add this new node to the treeview
        sub_level_item = QTreeWidgetItem(self.topLevelItem(index))
        sub_level_item.setText(0, obj_name)

    def __get_parent_index_from_name__(self, parent_node_name) -> int:
        for i in range(self.topLevelItemCount()):

            if self.topLevelItem(i).text(0) == parent_node_name:
                return i

        raise Exception("Couldn't find the parent node")

    def delete_node(self, node):
        node.parent().removeChild(node)

    def __get_childs_name__(self, parent_node_name) -> list:
        index = self.__get_parent_index_from_name__(parent_node_name)
        # Loop inside the child nodes to see if the name alreayd exists
        out_list = []
        for i in range(self.topLevelItem(index).childCount()):
            out_list.append(str(self.topLevelItem(index).child(i).text(0)))

        return out_list

    def get_checked_childs(self, parent_node_name):
        index = self.__get_parent_index_from_name__(parent_node_name)
        out_list = []
        for i in range(self.topLevelItem(index).childCount()):
            if self.topLevelItem(index).child(i).checkState(0) == QtCore.Qt.Checked:
                out_list.append(self.topLevelItem(index).child(i).text(0))

        return out_list

    def create_master_tree_nodes(self, parent_node_names: Globals.TreeViewNames):
        for i, results_dic in enumerate(parent_node_names):
            top_level_item = QtWidgets.QTreeWidgetItem(self)
            top_level_item.setText(0, results_dic.value)

    def load_child_nodes(self, project_directory, parent_node_names: Globals.TreeViewNames):
        for i, results_dic in enumerate(parent_node_names):
            obj_in_folder = os.listdir(project_directory + "\\" + results_dic.value)

            for name in obj_in_folder:
                sub_level_item = QtWidgets.QTreeWidgetItem(
                    self.topLevelItem(self.__get_parent_index_from_name__(results_dic.value)))
                obj_loaded = ProjectFramework("", "", "")
                obj_loaded.load_from_json(project_directory + "\\" + results_dic.value + "\\" + name)
                sub_level_item.setText(0, obj_loaded.name)

    def on_itemDoubleClicked(self, item):
        if (item.parent()):
            self.open_object(item)

    def menuContextTree(self, point):
        # Infos about the node selected.
        index = self.indexAt(point)

        if not index.isValid():
            return

        item = self.itemAt(point)
        name = item.text(0)  # The text of the node.

        if (not item.parent()):

            # We build the menu.
            menu = QtWidgets.QMenu()
            action_create = menu.addAction("Create")
            menu.addSeparator()
            action_import = menu.addAction("Import")

            action = menu.exec_(self.mapToGlobal(point))

            # Todo finish the correct actions to perform
            if action == action_create:
                self.create_object(item)
            elif action == action_import:
                pass

        else:

            # Todo finish the submenu for the items
            # We build the menu.
            menu = QtWidgets.QMenu()
            action_open = menu.addAction("Open")
            menu.addSeparator()
            action_duplicate = menu.addAction("Duplicate")
            action_rename = menu.addAction("Rename")
            menu.addSeparator()
            action_delete = menu.addAction("Delete")

            action = menu.exec_(self.mapToGlobal(point))

            if action == action_open:
                self.open_object(item)
            elif action == action_delete:
                self.delete_object(item)

    def create_object(self, node):
        if node.text(0) == Globals.TreeViewNames.aerodynamics.value:
            self.obj_form = Aerodynamics_W(None, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.text(0) == Globals.TreeViewNames.basic_properties.value:
            self.obj_form = BasicProperties_W(None, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.text(0) == Globals.TreeViewNames.inputs.value:
            self.obj_form = Inputs_W(None, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.text(0) == Globals.TreeViewNames.spring.value:
            self.obj_form = Spring_W(None, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.text(0) == Globals.TreeViewNames.ARB.value:
            self.obj_form = ARB_W(None, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.text(0) == Globals.TreeViewNames.tire.value:
            self.obj_form = Tire_W(None, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.text(0) == Globals.TreeViewNames.vehicle.value:
            self.obj_form = Vehicle_W(None, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.text(0) == Globals.TreeViewNames.half_suspension.value:
            self.obj_form = HalfSuspension_W(None, self.project_directory, self)
            self.obj_form.Form.show()

    # Todo Implemte the rest
    def open_object(self, node):
        if node.parent().text(0) == Globals.TreeViewNames.aerodynamics.value:
            obj_loaded = Aerodynamics_D()
            obj_loaded.load_object(
                self.project_directory + "\\" + Globals.TreeViewNames.aerodynamics.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form = Aerodynamics_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.parent().text(0) == Globals.TreeViewNames.basic_properties.value:
            obj_loaded = BasicProperties_D()
            obj_loaded.load_object(
                self.project_directory + "\\" + Globals.TreeViewNames.basic_properties.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form = BasicProperties_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.parent().text(0) == Globals.TreeViewNames.inputs.value:
            obj_loaded = Inputs_D()
            obj_loaded.load_object(
                self.project_directory + "\\" + Globals.TreeViewNames.inputs.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form = Inputs_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.parent().text(0) == Globals.TreeViewNames.spring.value:
            obj_loaded = Spring_D()
            obj_loaded.load_object(
                self.project_directory + "\\" + Globals.TreeViewNames.spring.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form = Spring_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.parent().text(0) == Globals.TreeViewNames.ARB.value:
            obj_loaded = ARB_D()
            obj_loaded.load_object(
                self.project_directory + "\\" + Globals.TreeViewNames.ARB.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form = ARB_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.parent().text(0) == Globals.TreeViewNames.tire.value:
            obj_loaded = Tire_D()
            obj_loaded.load_object(
                self.project_directory + "\\" + Globals.TreeViewNames.tire.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form = Tire_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.parent().text(0) == Globals.TreeViewNames.half_suspension.value:
            obj_loaded = HalfSuspension_D()
            obj_loaded.load_object(
                self.project_directory + "\\" + Globals.TreeViewNames.half_suspension.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form = HalfSuspension_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()

        elif node.parent().text(0) == Globals.TreeViewNames.vehicle.value:
            obj_loaded = Vehicle_D()
            obj_loaded.load_object(
                self.project_directory + "\\" + Globals.TreeViewNames.vehicle.value + "\\" + node.text(
                    0) + obj_loaded.extension)
            self.obj_form = Vehicle_W(obj_loaded, self.project_directory, self)
            self.obj_form.Form.show()

    def delete_object(self, node):
        self.delete_node(node)  # Delete the node
        self.project_framework.delete_file(self.project_directory, node.parent().text(0))  # Delete the file
