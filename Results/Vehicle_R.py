import csv

import numpy as np

from Managers.ProjectFramework import ProjectFramework


class Vehicle_R(ProjectFramework):
    """

    Creates an instance of the Aerodynamics_D class

    :param air_density: air density [Kg/m^3]
    :param downforce_distribution: Downforce distribution [float][0 - 1]
    :param frontal_area: Vehicle front area [m^2]
    :param downforce_coefficient: Downforce coefficient [int]
    :param name: Name of the object [str]

    Example::

        obj = Aerodynamics_D()
    """

    def __init__(self,
                 name="", np_array_size=0):
        ProjectFramework.__init__(self, name, ".WTres", "0.0.0")

        self.simulation_step = np.zeros(np_array_size)
        self.ay = np.zeros(np_array_size)
        self.ax = np.zeros(np_array_size)
        self.vx = np.zeros(np_array_size)

        self.non_susp_mass_front_WT = np.zeros(np_array_size)

        self.non_susp_mass_rear_WT = np.zeros(np_array_size)

        self.geometric_WT_front = np.zeros(np_array_size)

        self.geometric_WT_rear = np.zeros(np_array_size)

        self.front_spring_anti_roll_moment = np.zeros(np_array_size)

        self.rear_spring_anti_roll_moment = np.zeros(np_array_size)

        self.front_ARB_anti_roll_moment = np.zeros(np_array_size)

        self.rear_ARB_anti_roll_moment = np.zeros(np_array_size)

        self.front_total_anti_roll_moment = np.zeros(np_array_size)
        self.rear_total_anti_roll_moment = np.zeros(np_array_size)
        self.total_anti_roll_moment = np.zeros(np_array_size)

        self.elastic_WT_front = np.zeros(np_array_size)

        self.elastic_WT_rear = np.zeros(np_array_size)
        self.total_elastic_WT = np.zeros(np_array_size)
        self.total_geometric_WT = np.zeros(np_array_size)
        self.total_non_susp_mass_WT = np.zeros(np_array_size)

    def append_simulation_step_results(self, simulation, step):
        self.ay[step] = simulation._ay
        self.ax[step] = simulation._ax
        self.vx[step] = simulation._vx

        self.front_spring_anti_roll_moment[step] = simulation._front_spring_anti_roll_moment

        self.rear_spring_anti_roll_moment[step] = simulation._rear_spring_anti_roll_moment

        self.front_ARB_anti_roll_moment[step] = simulation._front_ARB_anti_roll_moment

        self.rear_ARB_anti_roll_moment[step] = simulation._rear_ARB_anti_roll_moment

        self.front_total_anti_roll_moment[step] = simulation._front_spring_anti_roll_moment
        self.rear_total_anti_roll_moment[step] = simulation._rear_total_anti_roll_moment
        self.total_anti_roll_moment[step] = simulation._total_anti_roll_moment

        self.elastic_WT_front[step] = simulation._elastic_WT_front
        self.elastic_WT_rear[step] = simulation._elastic_WT_rear
        self.total_elastic_WT[step] = simulation._total_elastic_WT

        self.non_susp_mass_front_WT[step] = simulation._non_susp_mass_front_WT
        self.non_susp_mass_rear_WT[step] = simulation._non_susp_mass_rear_WT
        self.total_non_susp_mass_WT[step] = simulation._total_non_susp_mass_WT

        self.geometric_WT_front[step] = simulation._geometric_WT_front
        self.geometric_WT_rear[step] = simulation._geometric_WT_rear
        self.total_geometric_WT[step] = simulation._total_geometric_WT

    # region save/load

    # Save object
    def save_object(self, file_path=""):
        """
        Saves this object into a JSON file into the directory specified in file_path

        :param file_path: file_path where the Json file is to be saved. Ex: "Desktop/User/" the name and extension are
        automatically added from the object properties self.name self.extension
        :return: Nothing

        .. warning::

            To save an object the self.name properties cannot be empty.

        Example::

            obj = Aerodynamics_D() #Create Class
            obj.init_default_values() #Init default values
            obj.name = "Example"  #A name for the object is necessary if you want to save it
            obj.save_object("C:\\Users\\joaop\\Desktop") #Save

        """

        # The local variable data, is a dictionary where we will be saving all the parameters that we want. Typically
        # this parameters will be the name, version, date, user (from the project Framework class), and the additional
        # parameters that are part of the class itself. After that the function will call the save_to_json function which
        # will save the data into a file.

        data = {
            "name": self.name,
            "version": self.version,
            "date": self.current_date_time(),
            "user": self.get_user(),
        }

        self.save_to_json(file_path, data)  # Call the json method to save the file
        self.save_results(file_path)

    def save_results(self, file_name=""):
        with open(file_name + "\\" + self.name + ".csv", "w", newline='') as file_to_read:
            csv_writer = csv.writer(file_to_read, delimiter=',', quoting=csv.QUOTE_MINIMAL)
            for i in range(len(self.ay)):
                csv_writer.writerow([float(self.simulation_step[i]),
                                     float(self.ax[i]),
                                     float(self.ay[i]),
                                     float(self.vx[i])])

    # Load Object
    def load_object(self, file):
        """
        Loads the values from a JSON file to the object

        :param file: name of the file with extension Ex: filename.ext
        :return: None

        In the following example we are going to load an object from the aerodynamics class.
        In this case it is necessary to first create the object and then issue the load_object function to load the
        values into the class

        Example::

            obj = Aerodynamics_D() #Create Class
            obj.load_object("C:\\Users\\joaop\\Desktop\\Example.aer") #Load

        """
        data = self.load_from_json(file)

        # Get the parameters from the json file.

    # endregion
