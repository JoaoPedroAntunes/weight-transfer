import csv

import numpy as np


class SimulationResults():
    def __init__(self, np_array_size):

        self.simulation_step = np.zeros(np_array_size)
        self.ay = np.zeros(np_array_size)
        self.ax = np.zeros(np_array_size)
        self.vx = np.zeros(np_array_size)

        self.non_susp_mass_front_WT = np.zeros(np_array_size)

        self.non_susp_mass_rear_WT = np.zeros(np_array_size)

        self.geometric_WT_front = np.zeros(np_array_size)

        self.geometric_WT_rear = np.zeros(np_array_size)

        self.front_spring_anti_roll_moment = np.zeros(np_array_size)

        self.rear_spring_anti_roll_moment = np.zeros(np_array_size)

        self.front_ARB_anti_roll_moment = np.zeros(np_array_size)

        self.rear_ARB_anti_roll_moment = np.zeros(np_array_size)

        self.front_total_anti_roll_moment = np.zeros(np_array_size)
        self.rear_total_anti_roll_moment = np.zeros(np_array_size)
        self.total_anti_roll_moment = np.zeros(np_array_size)

        self.elastic_WT_front = np.zeros(np_array_size)

        self.elastic_WT_rear = np.zeros(np_array_size)
        self.total_elastic_WT = np.zeros(np_array_size)
        self.total_geometric_WT = np.zeros(np_array_size)
        self.total_non_susp_mass_WT = np.zeros(np_array_size)

    def append_simulation_step_results(self, simulation, step):
        self.ay[step] = simulation._ay
        self.ax[step] = simulation._ax
        self.vx[step] = simulation._vx

        self.front_spring_anti_roll_moment[step] = simulation._front_spring_anti_roll_moment

        self.rear_spring_anti_roll_moment[step] = simulation._rear_spring_anti_roll_moment

        self.front_ARB_anti_roll_moment[step] = simulation._front_ARB_anti_roll_moment

        self.rear_ARB_anti_roll_moment[step] = simulation._rear_ARB_anti_roll_moment

        self.front_total_anti_roll_moment[step] = simulation._front_spring_anti_roll_moment
        self.rear_total_anti_roll_moment[step] = simulation._rear_total_anti_roll_moment
        self.total_anti_roll_moment[step] = simulation._total_anti_roll_moment

        self.elastic_WT_front[step] = simulation._elastic_WT_front
        self.elastic_WT_rear[step] = simulation._elastic_WT_rear
        self.total_elastic_WT[step] = simulation._total_elastic_WT

        self.non_susp_mass_front_WT[step] = simulation._non_susp_mass_front_WT
        self.non_susp_mass_rear_WT[step] = simulation._non_susp_mass_rear_WT
        self.total_non_susp_mass_WT[step] = simulation._total_non_susp_mass_WT

        self.geometric_WT_front[step] = simulation._geometric_WT_front
        self.geometric_WT_rear[step] = simulation._geometric_WT_rear
        self.total_geometric_WT[step] = simulation._total_geometric_WT

    def calculate_1st_magic_number(self):
        pass

    def calculate_2nd_magic_number(self):
        pass

    def calculate_3rd_magic_number(self):
        pass



    def print_results(self, index = None):
        if index is None:
            for i in range(len(self.non_susp_mass_front_WT)):

                print("Results from iteration: {}".format(i))
                print("")

                print("Non Suspended Mass Weight Transfer:")
                print("Front: {} [N]".format(self.non_susp_mass_front_WT[i]))
                print("Rear: {} [N]".format(self.non_susp_mass_rear_WT[i]))
                print("Total: {} [N]".format(self.non_susp_mass_front_WT[i] + self.non_susp_mass_rear_WT[i]))
                print("")

                print("Geometric Weight Transfer:")
                print("Front: {} [N]".format(self.geometric_WT_front[i]))
                print("Rear: {} [N]".format(self.geometric_WT_rear[i]))
                print("Total: {} [N]".format(self.geometric_WT_front[i] + self.geometric_WT_rear[i]))
                print("")

                print("Anti Roll Moment from Springs:")
                print("Front: {} [N]".format(self.front_spring_anti_roll_moment[i]))
                print("Rear: {} [N]".format(self.rear_spring_anti_roll_moment[i]))
                print("Total: {} [N]".format(self.front_spring_anti_roll_moment[i] + self.rear_spring_anti_roll_moment[i]))
                print("")

                print("Anti Roll Moment from ARB:")
                print("Front: {} [N]".format(self.front_ARB_anti_roll_moment[i]))
                print("Rear: {} [N]".format(self.rear_ARB_anti_roll_moment[i]))
                print("Total: {} [N]".format(self.front_ARB_anti_roll_moment[i] + self.rear_ARB_anti_roll_moment[i]))
                print("")

                print("Elastic Weight Transfer:")
                print("Front: {} [N]".format(self.elastic_WT_front[i]))
                print("Rear: {} [N]".format(self.elastic_WT_rear[i]))
                print("Total: {} [N]".format(self.total_elastic_WT[i]))
                print("")
                print("/////////////////////////////////////////////////////////////")

        else:
            print("Non Suspended Mass Weight Transfer:")
            print("Front: {} [N]".format(self.non_susp_mass_front_WT[index]))
            print("Rear: {} [N]".format(self.non_susp_mass_rear_WT[index]))
            print("Total: {} [N]".format(self.non_susp_mass_front_WT[index] + self.non_susp_mass_rear_WT[index]))
            print("")

            print("Geometric Weight Transfer:")
            print("Front: {} [N]".format(self.geometric_WT_front[index]))
            print("Rear: {} [N]".format(self.geometric_WT_rear[index]))
            print("Total: {} [N]".format(self.geometric_WT_front[index] + self.geometric_WT_rear[index]))
            print("")

            print("Anti Roll Moment from Springs:")
            print("Front: {} [N]".format(self.front_spring_anti_roll_moment[index]))
            print("Rear: {} [N]".format(self.rear_spring_anti_roll_moment[index]))
            print("Total: {} [N]".format(
                self.front_spring_anti_roll_moment[index] + self.rear_spring_anti_roll_moment[index]))
            print("")

            print("Anti Roll Moment from ARB:")
            print("Front: {} [N]".format(self.front_ARB_anti_roll_moment[index]))
            print("Rear: {} [N]".format(self.rear_ARB_anti_roll_moment[index]))
            print("Total: {} [N]".format(
                self.front_ARB_anti_roll_moment[index] + self.rear_ARB_anti_roll_moment[index]))
            print("")

            print("Elastic Weight Transfer:")
            print("Front: {} [N]".format(self.elastic_WT_front[index]))
            print("Rear: {} [N]".format(self.elastic_WT_rear[index]))
            print("Total: {} [N]".format(self.total_elastic_WT[index]))
            print("")

    def save_results(self, file_name=""):
        with open(file_name, "w", newline='') as file_to_read:
            csv_writer = csv.writer(file_to_read, delimiter=',', quoting=csv.QUOTE_MINIMAL)
            for i in range(len(self.ay)):
                csv_writer.writerow([float(self.simulation_step[i]),
                                     float(self.ax[i]),
                                     float(self.ay[i]),
                                     float(self.vx[i])])

        """
        
        self.front_ARB_anti_roll_moment[step] = simulation._front_ARB_anti_roll_moment

        self.rear_ARB_anti_roll_moment[step] = simulation._rear_ARB_anti_roll_moment

        self.front_total_anti_roll_moment[step] = simulation._front_spring_anti_roll_moment
        self.rear_total_anti_roll_moment[step] = simulation._rear_total_anti_roll_moment
        self.total_anti_roll_moment[step] = simulation._total_anti_roll_moment

        self.elastic_WT_front[step] = simulation._elastic_WT_front
        self.elastic_WT_rear[step] = simulation._elastic_WT_rear
        self.total_elastic_WT[step] = simulation._total_elastic_WT

        self.non_susp_mass_front_WT[step] = simulation._non_susp_mass_front_WT
        self.non_susp_mass_rear_WT[step] = simulation._non_susp_mass_rear_WT
        self.total_non_susp_mass_WT[step] = simulation._total_non_susp_mass_WT

        self.geometric_WT_front[step] = simulation._geometric_WT_front
        self.geometric_WT_rear[step] = simulation._geometric_WT_rear
        self.total_geometric_WT[step] = simulation._total_geometric_WT
        
        """
