from PyQt5 import QtWidgets, QtCore

from Globals import Globals
from Managers.TreeWidgetManager import TreeWidgetManager
from UserInterface.UI.RunSimulation_ui import Ui_RunSimulation


class GetUserSimulation_W:
    def __init__(self, tree_widget):
        self.Form = QtWidgets.QDialog()
        self.ui = Ui_RunSimulation()
        self.ui.setupUi(self.Form)


        self.tree_widget = tree_widget
        self.ui.treeView_Inputs = TreeWidgetManager(self.ui.groupBox)
        self.ui.treeView_Inputs.setObjectName("treeView_Inputs")
        self.ui.treeView_Inputs.headerItem().setText(0, "1")
        self.ui.treeView_Inputs.header().setVisible(False)
        self.ui.horizontalLayout_2.addWidget(self.ui.treeView_Inputs)

        self.ui.treeView_Vehicle = TreeWidgetManager(self.ui.groupBox)
        self.ui.treeView_Vehicle.setObjectName("treeView_Vehicle")
        self.ui.treeView_Vehicle.headerItem().setText(0, "1")
        self.ui.treeView_Vehicle.header().setVisible(False)
        self.ui.horizontalLayout_2.addWidget(self.ui.treeView_Vehicle)

        input_list = self.tree_widget.__get_childs_name__(Globals.TreeViewNames.inputs.value)
        vehicle_list = self.tree_widget.__get_childs_name__(Globals.TreeViewNames.vehicle.value)
        top_level_item = QtWidgets.QTreeWidgetItem(self.ui.treeView_Inputs)
        top_level_item.setText(0, Globals.TreeViewNames.inputs.value)
        for i, res_dic in enumerate(input_list):
            sub_level_item = QtWidgets.QTreeWidgetItem(top_level_item)
            sub_level_item.setText(0, res_dic)
            sub_level_item.setFlags(sub_level_item.flags() | QtCore.Qt.ItemIsUserCheckable)
            sub_level_item.setCheckState(0, QtCore.Qt.Unchecked)

        top_level_item2 = QtWidgets.QTreeWidgetItem(self.ui.treeView_Vehicle)
        top_level_item2.setText(0, Globals.TreeViewNames.vehicle.value)
        for i, res_dic in enumerate(vehicle_list):
            sub_level_item2 = QtWidgets.QTreeWidgetItem(top_level_item2)
            sub_level_item2.setText(0, res_dic)
            sub_level_item2.setFlags(sub_level_item2.flags() | QtCore.Qt.ItemIsUserCheckable)
            sub_level_item2.setCheckState(0, QtCore.Qt.Unchecked)

        self.ui.treeView_Inputs.expandAll()
        self.ui.treeView_Vehicle.expandAll()

        self.ui.pushButton_OK.clicked.connect(self.on_pushButton_OK_clicked)

    def on_pushButton_OK_clicked(self):
        self.vehicle_list = self.ui.treeView_Vehicle.get_checked_childs(Globals.TreeViewNames.vehicle.value)
        self.input_list = self.ui.treeView_Inputs.get_checked_childs(Globals.TreeViewNames.inputs.value)
        self.Form.close()
