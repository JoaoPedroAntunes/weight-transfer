from PyQt5 import QtWidgets

from Design.Inputs_D import Inputs_D
from Design.Vehicle_D import Vehicle_D
from Managers.ProjectFramework import DesignDirectory
from Solver import Vehicle_S, Inputs_S
from UserInterface.UI.SimulationSolver_ui import Ui_SimulationSolver
from WeightTransferSimulation import WeightTransferSimulation


class RunSimulation_W:
    def __init__(self, project_directory, input_list_out, vehicle_list_out):
        self.Form = QtWidgets.QDialog()
        self.ui = Ui_SimulationSolver()
        self.ui.setupUi(self.Form)

        self.ui.pushButton.clicked.connect(self.on_pushButton_clicked)
        self.ui.pushButton.setEnabled(False)

        self.project_directory = project_directory
        vehicle_list_S = []
        for vehicle_name in vehicle_list_out:
            veh_D = Vehicle_D()
            veh_D.load_object(
                project_directory + "\\" + DesignDirectory.Vehicle.value + "\\" + vehicle_name + veh_D.extension)
            veh_S = Vehicle_S(veh_D)
            vehicle_list_S.append(veh_S)

        inp_S = []
        for input_name in input_list_out:
            inp_D = Inputs_D()
            inp_D.load_object(
                project_directory + "\\" + DesignDirectory.inputs.value + "\\" + input_name + inp_D.extension)
            inpa_S = Inputs_S(inp_D)
            inp_S.append(inpa_S)

        for i in range(len(vehicle_list_S)):
            self.ui.textEdit.append("Running Vehicle Model: {}".format(i))

            for j in range(len(inp_S)):
                self.ui.textEdit.append("     Running Simulation Input: {}".format(j))
                sim_man = WeightTransferSimulation()

                for k in range(len(inp_S[j].lateral_acceleration)):
                    loc = Inputs_S()
                    loc.lateral_acceleration = inp_S[j].lateral_acceleration[k]
                    loc.longitudinal_acceleration = inp_S[j].longitudinal_acceleration[k]
                    loc.longitudinal_velocity = inp_S[j].longitudinal_velocity[k]

                    self.ui.textEdit.append("         Running step: {}".format(k))
                    sim_man.run_step(vehicle_list_S[i], loc)

        # sim_man = SimulationManager(vehicle_list_S, inp_S)

        # sim_man.run()

        self.ui.textEdit.append("Simulation Complete")
        self.ui.pushButton.setEnabled(True)
        self.ui.pushButton.setText("Close")

    def on_pushButton_clicked(self):
        self.Form.close()
