from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox

from Design.BasicProperties_D import BasicProperties_D
from Globals import Globals
from UserInterface.UI.basic_properties_ui import Ui_BasicPropertiesForm


class BasicProperties_W:
    def __init__(self, basic_properties_D, project_directory, tree_widget):

        self.Form = QtWidgets.QWidget()
        self.ui = Ui_BasicPropertiesForm()
        self.ui.setupUi(self.Form)
        self.basic_properties_D = basic_properties_D
        self.project_directory = project_directory
        self.tree_widget = tree_widget

        if self.basic_properties_D is None:
            self.ui.lineEdit.setText("")
            self.ui.lineEdit_3.setText("")
            self.ui.lineEdit_6.setText("")
            self.ui.lineEdit_7.setText("")
            self.ui.lineEdit_Name.setText("")

            # Flag to know that we are creating a new object. Initialize an empty object
            self.is_new_obj = True
            self.basic_properties_D = BasicProperties_D()

        else:
            self.ui.lineEdit.setText(str(self.basic_properties_D.weight))
            self.ui.lineEdit_3.setText(str(self.basic_properties_D.wheelbase))
            self.ui.lineEdit_6.setText(str(self.basic_properties_D.suspended_weight_distribution))
            self.ui.lineEdit_7.setText(str(self.basic_properties_D.suspended_weight_CG))
            self.ui.lineEdit_Name.setText(str(self.basic_properties_D.name))

            self.is_new_obj = False

        self.ui.pushButton_OK.clicked.connect(self.on_pushButton_OK_clicked)

    def on_pushButton_OK_clicked(self):

        # Pass the values from the form to the object
        self.basic_properties_D.weight = float(self.ui.lineEdit.text())
        self.basic_properties_D.wheelbase = float(self.ui.lineEdit_3.text())
        self.basic_properties_D.suspended_weight_distribution = float(self.ui.lineEdit_6.text())
        self.basic_properties_D.suspended_weight_CG = float(self.ui.lineEdit_7.text())
        self.basic_properties_D.name = str(self.ui.lineEdit_Name.text())

        if self.is_new_obj:

            # If there is no duplicated name then save
            if not self.tree_widget.is_name_already_exists(Globals.TreeViewNames.basic_properties.value,
                                                           self.basic_properties_D.name):
                self.basic_properties_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.basic_properties.value)
                self.tree_widget.add_obj_to_tree(self.basic_properties_D.name, Globals.TreeViewNames.basic_properties.value)
                self.Form.close()
            # If there is a duplicated name then display a box to ask what the user wants to do>
            # Override, cancel or discard.
            else:
                ret = self.tree_widget.msg_box_name_already_exists()
                if ret == QMessageBox.Save:
                    self.basic_properties_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.basic_properties.value)
                    self.Form.close()
                elif ret == QMessageBox.Discard:
                    self.Form.close()
                elif ret == QMessageBox.Cancel:
                    pass
        else:  # If it's not a new object then simply save it
            self.basic_properties_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.basic_properties.value)
            self.Form.close()


if __name__ == "__main__":
    foo = BasicProperties_D()
    foo.init_default_values()
    foo.name = "Asdasd"

    foo_w = BasicProperties_W(foo, None, "")
