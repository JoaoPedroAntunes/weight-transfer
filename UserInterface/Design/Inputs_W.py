import numpy as np
from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox, QTableWidgetItem

from Design.Inputs_D import Inputs_D
from Globals import Globals
from UserInterface.UI import Ui_Inputs


class Inputs_W:
    def __init__(self, inputs_D, project_directory, tree_widget):
        self.Form = QtWidgets.QWidget()
        self.ui = Ui_Inputs()
        self.ui.setupUi(self.Form)
        self.inputs_D = inputs_D
        self.project_directory = project_directory
        self.tree_widget = tree_widget

        # Init QTableWidget
        self.ui.tableWidget_inputs.setRowCount(1)
        self.ui.tableWidget_inputs.setColumnCount(3)
        self.ui.tableWidget_inputs.setHorizontalHeaderItem(0, QTableWidgetItem("Longitudinal \n Acceleration"))
        self.ui.tableWidget_inputs.setHorizontalHeaderItem(1, QTableWidgetItem("Lateral \n Acceleration"))
        self.ui.tableWidget_inputs.setHorizontalHeaderItem(2, QTableWidgetItem("Longitudinal \n Velocity"))

        if self.inputs_D is None:


            # Flag to know that we are creating a new object. Initialize an empty object
            self.is_new_obj = True
            self.inputs_D = Inputs_D()
        else:
            # Todo add here for the table
            for i in range(len(self.inputs_D.lateral_acceleration)):
                self.ui.tableWidget_inputs.insertRow(self.ui.tableWidget_inputs.rowCount())
                foo = self.inputs_D.longitudinal_acceleration[i]
                self.ui.tableWidget_inputs.setItem(i, 0,
                                                   QTableWidgetItem(str(self.inputs_D.longitudinal_acceleration[i])))
                self.ui.tableWidget_inputs.setItem(i, 1, QTableWidgetItem(str(self.inputs_D.lateral_acceleration[i])))
                self.ui.tableWidget_inputs.setItem(i, 2, QTableWidgetItem(str(self.inputs_D.longitudinal_velocity[i])))

            self.ui.lineEdit_Name.setText(str(self.inputs_D.name))

            self.is_new_obj = False

        self.ui.pushButton_OK.clicked.connect(self.on_pushButton_OK_clicked)
        self.ui.pushButton_inputs_add.clicked.connect(self.on_pushButton_add_input_clicked)

    def on_pushButton_add_input_clicked(self):
        longitudinal_acceleration = str(self.ui.lineEdit_long_acc_y.text())
        lateral_acceleration = str(self.ui.lineEdit_lat_acc_y.text())
        longitudinal_velocity = str(self.ui.lineEdit_long_velocity_y.text())

        self.ui.tableWidget_inputs.setItem(self.ui.tableWidget_inputs.rowCount() - 1, 0,
                                           QTableWidgetItem(longitudinal_acceleration))

        self.ui.tableWidget_inputs.setItem(self.ui.tableWidget_inputs.rowCount() - 1, 1,
                                           QTableWidgetItem(lateral_acceleration))

        self.ui.tableWidget_inputs.setItem(self.ui.tableWidget_inputs.rowCount() - 1, 2,
                                           QTableWidgetItem(longitudinal_velocity))

        self.ui.tableWidget_inputs.insertRow(self.ui.tableWidget_inputs.rowCount())

    def on_pushButton_OK_clicked(self):
        self.inputs_D.name = str(self.ui.lineEdit_Name.text())

        lateral_acceleration = np.zeros((self.ui.tableWidget_inputs.rowCount() - 1))
        longitudinal_acceleration = np.zeros((self.ui.tableWidget_inputs.rowCount() - 1))
        longitudinal_velocity = np.zeros((self.ui.tableWidget_inputs.rowCount() - 1))
        for i in range(self.ui.tableWidget_inputs.rowCount() - 1):
            longitudinal_acceleration[i] = self.ui.tableWidget_inputs.item(i, 0).text()
            lateral_acceleration[i] = self.ui.tableWidget_inputs.item(i, 1).text()
            longitudinal_velocity[i] = self.ui.tableWidget_inputs.item(i, 2).text()

        self.inputs_D.longitudinal_acceleration = longitudinal_acceleration
        self.inputs_D.lateral_acceleration = lateral_acceleration
        self.inputs_D.longitudinal_velocity = longitudinal_velocity

        # Todo add here for the table
        # self.ui.lineEdit_Stiffness.setText(str(self.tire_D.stiffness))
        # self.ui.lineEdit_Name.setText(str(self.tire_D.name))

        if self.is_new_obj:

            # If there is no duplicated name then save
            if not self.tree_widget.is_name_already_exists(Globals.TreeViewNames.inputs.value,
                                                           self.inputs_D.name):
                self.inputs_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.inputs.value)
                self.tree_widget.add_obj_to_tree(self.inputs_D.name, Globals.TreeViewNames.inputs.value)
                self.Form.close()
            # If there is a duplicated name then display a box to ask what the user wants to do>
            # Override, cancel or discard.
            else:
                ret = self.tree_widget.msg_box_name_already_exists()
                if ret == QMessageBox.Save:
                    self.inputs_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.inputs.value)
                    self.Form.close()
                elif ret == QMessageBox.Discard:
                    self.Form.close()
                elif ret == QMessageBox.Cancel:
                    pass
        else:  # If it's not a new object then simply save it
            self.inputs_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.inputs.value)
            self.Form.close()
