from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox

from Design.Vehicle_D import Vehicle_D
from Globals import Globals
from UserInterface.UI import Ui_Vehicle


class Vehicle_W:
    def __init__(self, vehicle_D: Vehicle_D = None, project_directory: str = "", tree_widget=None):

        self.Form = QtWidgets.QWidget()
        self.ui = Ui_Vehicle()
        self.ui.setupUi(self.Form)
        self.project_directory = project_directory
        self.tree_widget = tree_widget


        # Loads the comboboxes with the names in the treenodes.
        self.__load_tree_nodes_to_combo_box__()

        if vehicle_D is None:
            self.vehicle_D = Vehicle_D()
            self.is_new_obj = True

        else:
            self.is_new_obj = False
            self.vehicle_D = vehicle_D

            # Check if the name exists in the treeview and select that on the comboBox
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_aerodynamics, self.vehicle_D.aerodynamics.name)
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_basic_properties, self.vehicle_D.basic_properties.name)
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_front_suspension, self.vehicle_D.suspension.front.name)
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_rear_suspension, self.vehicle_D.suspension.rear.name)
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_front_ARB, self.vehicle_D.suspension.front.ARB.name)
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_rear_ARB, self.vehicle_D.suspension.rear.ARB.name)
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_front_left_spring,
                                               self.vehicle_D.suspension.front.left.spring.name)
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_front_right_spring,
                                               self.vehicle_D.suspension.front.right.spring.name)
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_rear_left_spring,
                                               self.vehicle_D.suspension.rear.left.spring.name)
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_rear_right_spring,
                                               self.vehicle_D.suspension.rear.right.spring.name)
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_front_left_tire,
                                               self.vehicle_D.suspension.front.left.tire.name)
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_front_right_tire,
                                               self.vehicle_D.suspension.front.right.tire.name)
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_rear_left_tire,
                                               self.vehicle_D.suspension.rear.left.tire.name)
            self.__set_obj_name_to_combo_box__(self.ui.comboBox_rear_right_tire,
                                               self.vehicle_D.suspension.rear.right.tire.name)
            self.ui.lineEdit_Name.setText(str(self.vehicle_D.name))

        self.ui.pushButton_OK.clicked.connect(self.on_pushButton_OK_clicked)

    def __set_obj_name_to_combo_box__(self, combo_box, obj_name):
        index = combo_box.findText(obj_name)
        if combo_box.findText(obj_name) == -1:
            combo_box.setCurrentIndex(0)
        else:
            combo_box.setCurrentIndex(index)


    def __load_tree_nodes_to_combo_box__(self):
        # Add first and empty option so that in case the object doesn't exist it goes to an empty option
        self.ui.comboBox_aerodynamics.addItem("")
        self.ui.comboBox_basic_properties.addItem("")
        self.ui.comboBox_front_suspension.addItem("")
        self.ui.comboBox_rear_suspension.addItem("")
        self.ui.comboBox_front_ARB.addItem("")
        self.ui.comboBox_rear_ARB.addItem("")
        self.ui.comboBox_front_left_spring.addItem("")
        self.ui.comboBox_front_right_spring.addItem("")
        self.ui.comboBox_rear_left_spring.addItem("")
        self.ui.comboBox_rear_right_spring.addItem("")
        self.ui.comboBox_front_left_tire.addItem("")
        self.ui.comboBox_front_right_tire.addItem("")
        self.ui.comboBox_rear_left_tire.addItem("")
        self.ui.comboBox_rear_right_tire.addItem("")

        # Loop through the master nodes
        for i in range(self.tree_widget.topLevelItemCount()):
            if self.tree_widget.topLevelItem(i).text(0) == Globals.TreeViewNames.aerodynamics.value:
                for j in range(self.tree_widget.topLevelItem(i).childCount()):
                    self.ui.comboBox_aerodynamics.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))

            elif self.tree_widget.topLevelItem(i).text(0) == Globals.TreeViewNames.basic_properties.value:
                for j in range(self.tree_widget.topLevelItem(i).childCount()):
                    self.ui.comboBox_basic_properties.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))

            elif self.tree_widget.topLevelItem(i).text(0) == Globals.TreeViewNames.half_suspension.value:
                for j in range(self.tree_widget.topLevelItem(i).childCount()):
                    self.ui.comboBox_front_suspension.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))
                    self.ui.comboBox_rear_suspension.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))

            elif self.tree_widget.topLevelItem(i).text(0) == Globals.TreeViewNames.ARB.value:
                for j in range(self.tree_widget.topLevelItem(i).childCount()):
                    self.ui.comboBox_front_ARB.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))
                    self.ui.comboBox_rear_ARB.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))

            elif self.tree_widget.topLevelItem(i).text(0) == Globals.TreeViewNames.spring.value:
                for j in range(self.tree_widget.topLevelItem(i).childCount()):
                    self.ui.comboBox_front_left_spring.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))
                    self.ui.comboBox_front_right_spring.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))
                    self.ui.comboBox_rear_left_spring.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))
                    self.ui.comboBox_rear_right_spring.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))

            elif self.tree_widget.topLevelItem(i).text(0) == Globals.TreeViewNames.tire.value:
                for j in range(self.tree_widget.topLevelItem(i).childCount()):
                    self.ui.comboBox_front_left_tire.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))
                    self.ui.comboBox_front_right_tire.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))
                    self.ui.comboBox_rear_left_tire.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))
                    self.ui.comboBox_rear_right_tire.addItem(str(self.tree_widget.topLevelItem(i).child(j).text(0)))

    def on_pushButton_OK_clicked(self):
        # Properties
        self.vehicle_D.basic_properties.name = str(self.ui.comboBox_basic_properties.currentText())

        # Aero
        self.vehicle_D.aerodynamics.name = str(self.ui.comboBox_aerodynamics.currentText())

        # Suspension
        self.vehicle_D.suspension.front.name = str(self.ui.comboBox_front_suspension.currentText())
        self.vehicle_D.suspension.rear.name = str(self.ui.comboBox_rear_suspension.currentText())

        # ARB
        self.vehicle_D.suspension.front.ARB.name = str(self.ui.comboBox_front_ARB.currentText())
        self.vehicle_D.suspension.rear.ARB.name = str(self.ui.comboBox_rear_ARB.currentText())

        # Springs
        self.vehicle_D.suspension.front.left.spring.name = str(self.ui.comboBox_front_left_spring.currentText())
        self.vehicle_D.suspension.front.right.spring.name = str(self.ui.comboBox_front_right_spring.currentText())
        self.vehicle_D.suspension.rear.left.spring.name = str(self.ui.comboBox_rear_left_spring.currentText())
        self.vehicle_D.suspension.rear.right.spring.name = str(self.ui.comboBox_rear_right_spring.currentText())

        # Tires
        self.vehicle_D.suspension.front.left.tire.name = str(self.ui.comboBox_front_left_tire.currentText())
        self.vehicle_D.suspension.front.right.tire.name = str(self.ui.comboBox_front_right_tire.currentText())
        self.vehicle_D.suspension.rear.left.tire.name = str(self.ui.comboBox_rear_left_tire.currentText())
        self.vehicle_D.suspension.rear.right.tire.name = str(self.ui.comboBox_rear_right_tire.currentText())

        # Obj name
        self.vehicle_D.name = str(self.ui.lineEdit_Name.text())

        if self.is_new_obj:

            # If there is no duplicated name then save
            if not self.tree_widget.is_name_already_exists(Globals.TreeViewNames.vehicle.value,
                                                           self.vehicle_D.name):
                self.vehicle_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.vehicle.value)
                self.tree_widget.add_obj_to_tree(self.vehicle_D.name, Globals.TreeViewNames.vehicle.value)
                self.Form.close()
            # If there is a duplicated name then display a box to ask what the user wants to do>
            # Override, cancel or discard.
            else:
                ret = self.tree_widget.msg_box_name_already_exists()
                if ret == QMessageBox.Save:
                    self.vehicle_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.vehicle.value)
                    self.Form.close()
                elif ret == QMessageBox.Discard:
                    self.Form.close()
                elif ret == QMessageBox.Cancel:
                    pass
        else:  # If it's not a new object then simply save it
            self.vehicle_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.vehicle.value)
            self.Form.close()




if __name__ == "__main__":
    foo = Vehicle_D()
    foo.init_default_values()
    foo.name = "Vehicle1"

    foo_w = Vehicle_W()
