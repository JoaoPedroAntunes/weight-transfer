from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox
from sympy import Point3D

from Design.HalfSuspension_D import HalfSuspension_D
from Globals import Globals
from UserInterface.UI import Ui_HalfSuspension


class HalfSuspension_W:
    def __init__(self, suspension_D, project_directory, tree_widget):
        self.Form = QtWidgets.QWidget()
        self.ui = Ui_HalfSuspension()
        self.ui.setupUi(self.Form)
        self.suspension_D = suspension_D
        self.project_directory = project_directory
        self.tree_widget = tree_widget

        if self.suspension_D is None:
            self.ui.lineEdit_track.setText("")
            self.ui.lineEdit_roll_center.setText("")
            self.ui.lineEdit_non_suspended_mass_CG.setText("")
            self.ui.lineEdit_Name.setText("")

            # Flag to know that we are creating a new object. Initialize an empty object
            self.is_new_obj = True
            self.suspension_D = HalfSuspension_D()
        else:

            self.ui.lineEdit_track.setText(str(self.suspension_D.track))
            self.ui.lineEdit_roll_center.setText(str(float(self.suspension_D.roll_center.z)))
            self.ui.lineEdit_non_suspended_mass_CG.setText(str(self.suspension_D.non_susp_mass_CG))
            self.ui.lineEdit_Name.setText(str(self.suspension_D.name))

            self.is_new_obj = False

        self.ui.pushButton_OK.clicked.connect(self.on_pushButton_OK_clicked)

    def on_pushButton_OK_clicked(self):

        # Pass the values from the form to the object
        self.suspension_D.track = float(self.ui.lineEdit_track.text())
        self.suspension_D.roll_center = Point3D(0, 0, float(self.ui.lineEdit_roll_center.text()))
        self.suspension_D.non_susp_mass_CG = float(self.ui.lineEdit_non_suspended_mass_CG.text())
        self.suspension_D.name = str(self.ui.lineEdit_Name.text())

        if self.is_new_obj:

            # If there is no duplicated name then save
            if not self.tree_widget.is_name_already_exists(Globals.TreeViewNames.half_suspension.value,
                                                           self.suspension_D.name):
                self.suspension_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.half_suspension.value)
                self.tree_widget.add_obj_to_tree(self.suspension_D.name, Globals.TreeViewNames.half_suspension.value)
                self.Form.close()
            # If there is a duplicated name then display a box to ask what the user wants to do>
            # Override, cancel or discard.
            else:
                ret = self.tree_widget.msg_box_name_already_exists()
                if ret == QMessageBox.Save:
                    self.suspension_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.half_suspension.value)
                    self.Form.close()
                elif ret == QMessageBox.Discard:
                    self.Form.close()
                elif ret == QMessageBox.Cancel:
                    pass
        else:  # If it's not a new object then simply save it
            self.suspension_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.half_suspension.value)
            self.Form.close()


if __name__ == "__main__":
    foo = HalfSuspension_D()
    foo.init_default_values()
    foo.name = "Suspension1"

    foo_w = HalfSuspension_W(foo, None, "")
