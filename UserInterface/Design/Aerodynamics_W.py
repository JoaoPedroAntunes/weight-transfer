import sys

from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox

from Design.Aerodynamics_D import Aerodynamics_D
from Globals import Globals
from UserInterface.UI import Ui_Aerodynamics


class Aerodynamics_W:
    """

    Creates an instance of the Aerodynamics_D class

    :param air_density: air density [Kg/m^3]
    :param downforce_distribution: Downforce distribution [float][0 - 1]
    :param frontal_area: Vehicle front area [m^2]
    :param downforce_coefficient: Downforce coefficient [int]
    :param name: Name of the object [str]

    Example::

        obj = Aerodynamics_D()
    """

    def __init__(self, aerodynamics_D, project_directory, tree_widget):

        self.Form = QtWidgets.QWidget()
        self.ui = Ui_Aerodynamics()
        self.ui.setupUi(self.Form)
        self.aerodynamics_D = aerodynamics_D
        self.project_directory = project_directory
        self.tree_widget = tree_widget

        if self.aerodynamics_D is None:
            self.ui.lineEdit_air_density.setText("")
            self.ui.lineEdit_downforce_distribution.setText("")
            self.ui.lineEdit_frontal_area.setText("")
            self.ui.lineEdit_downforce_coefficient.setText("")
            self.ui.lineEdit_Name.setText("")

            # Flag to know that we are creating a new object. Initialize an empty object
            self.is_new_obj = True
            self.aerodynamics_D = Aerodynamics_D()

        else:

            self.ui.lineEdit_air_density.setText(str(self.aerodynamics_D.air_density))
            self.ui.lineEdit_downforce_distribution.setText(str(self.aerodynamics_D.downforce_distribution))
            self.ui.lineEdit_frontal_area.setText(str(self.aerodynamics_D.frontal_area))
            self.ui.lineEdit_downforce_coefficient.setText(str(self.aerodynamics_D.downforce_coefficient))
            self.ui.lineEdit_Name.setText(str(self.aerodynamics_D.name))

            self.is_new_obj = False

        self.ui.pushButton_OK.clicked.connect(self.on_pushButton_OK_clicked)

    def on_pushButton_OK_clicked(self):

        # Pass the values from the form to the object
        self.aerodynamics_D.air_density = float(self.ui.lineEdit_air_density.text())
        self.aerodynamics_D.downforce_distribution = float(self.ui.lineEdit_downforce_distribution.text())
        self.aerodynamics_D.frontal_area = float(self.ui.lineEdit_frontal_area.text())
        self.aerodynamics_D.downforce_coefficient = float(self.ui.lineEdit_downforce_coefficient.text())
        self.aerodynamics_D.name = str(self.ui.lineEdit_Name.text())

        if self.is_new_obj:

            # If there is no duplicated name then save
            if not self.tree_widget.is_name_already_exists(Globals.TreeViewNames.aerodynamics.value,
                                                           self.aerodynamics_D.name):
                self.aerodynamics_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.aerodynamics.value)
                self.tree_widget.add_obj_to_tree(self.aerodynamics_D, Globals.TreeViewNames.aerodynamics.value)
                self.Form.close()
            # If there is a duplicated name then display a box to ask what the user wants to do>
            # Override, cancel or discard.
            else:
                ret = self.tree_widget.msg_box_name_already_exists()
                if ret == QMessageBox.Save:
                    self.aerodynamics_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.aerodynamics.value)
                    self.Form.close()
                elif ret == QMessageBox.Discard:
                    self.Form.close()
                elif ret == QMessageBox.Cancel:
                    pass
        else:  # If it's not a new object then simply save it
            self.aerodynamics_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.aerodynamics.value)
            self.Form.close()

if __name__ == "__main__":
    app = QtWidgets.QApplication(sys.argv)
    Form = QtWidgets.QWidget()
    ui = Ui_Aerodynamics()
    ui.setupUi(Form)

    foo = Aerodynamics_D()
    foo.init_default_values()
    foo.name = "Aero1"

    foo_w = Aerodynamics_W(foo, "", None)
    Form.show()
    sys.exit(app.exec_())
