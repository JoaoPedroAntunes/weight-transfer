from PyQt5 import QtWidgets
from PyQt5.QtWidgets import QMessageBox

from Design import ARB_D
from Globals import Globals
from UserInterface.UI import Ui_ARB


class ARB_W:
    def __init__(self, arb_D, project_directory, tree_widget):

        self.Form = QtWidgets.QWidget()
        self.ui = Ui_ARB()
        self.ui.setupUi(self.Form)
        self.ARB_D = arb_D
        self.project_directory = project_directory
        self.tree_widget = tree_widget

        if self.ARB_D is None:
            self.ui.lineEdit_stiffness.setText("")
            self.ui.lineEdit_motion_ratio.setText("")
            self.ui.lineEdit_Name.setText("")

            # Flag to know that we are creating a new object. Initialize an empty object
            self.is_new_obj = True
            self.ARB_D = ARB_D()

        else:

            self.ui.lineEdit_stiffness.setText(str(self.ARB_D.stiffness))
            self.ui.lineEdit_motion_ratio.setText(str(self.ARB_D.motion_ratio))
            self.ui.lineEdit_Name.setText(str(self.ARB_D.name))

            self.is_new_obj = False

        self.ui.pushButton_OK.clicked.connect(self.on_pushButton_OK_clicked)

    def on_pushButton_OK_clicked(self):
        # Pass the values from the form to the object
        self.ARB_D.stiffness = float(self.ui.lineEdit_stiffness.text())
        self.ARB_D.motion_ratio = float(self.ui.lineEdit_motion_ratio.text())
        self.ARB_D.name = str(self.ui.lineEdit_Name.text())

        if self.is_new_obj:
            # If there is no duplicated name then save
            if not self.tree_widget.is_name_already_exists(Globals.TreeViewNames.ARB.value,
                                                           self.ARB_D.name):
                self.ARB_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.ARB.value)
                self.tree_widget.add_obj_to_tree(self.ARB_D.name, Globals.TreeViewNames.ARB.value)
                self.Form.close()
            # If there is a duplicated name then display a box to ask what the user wants to do>
            # Override, cancel or discard.
            else:
                ret = self.tree_widget.msg_box_name_already_exists()
                if ret == QMessageBox.Save:
                    self.ARB_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.ARB.value)
                    self.Form.close()
                elif ret == QMessageBox.Discard:
                    self.Form.close()
                elif ret == QMessageBox.Cancel:
                    pass

        else:  # If it's not a new object then simply save it
            self.ARB_D.save_object(self.project_directory + "/" + Globals.TreeViewNames.ARB.value)
            self.Form.close()


if __name__ == "__main__":
    foo = ARB_D()
    foo.init_default_values()
    foo.name = "ARB1"

    foo_w = ARB_W(foo, "", None)
