# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'SimulationSolver_ui.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtWidgets


class Ui_SimulationSolver(object):
    def setupUi(self, SimulationSolver):
        SimulationSolver.setObjectName("SimulationSolver")
        SimulationSolver.resize(640, 480)
        self.verticalLayout = QtWidgets.QVBoxLayout(SimulationSolver)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox_2 = QtWidgets.QGroupBox(SimulationSolver)
        self.groupBox_2.setMaximumSize(QtCore.QSize(16777215, 50))
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.progressBar = QtWidgets.QProgressBar(self.groupBox_2)
        self.progressBar.setProperty("value", 24)
        self.progressBar.setObjectName("progressBar")
        self.horizontalLayout.addWidget(self.progressBar)
        self.verticalLayout.addWidget(self.groupBox_2)
        self.groupBox = QtWidgets.QGroupBox(SimulationSolver)
        self.groupBox.setObjectName("groupBox")
        self.verticalLayout_2 = QtWidgets.QVBoxLayout(self.groupBox)
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.textEdit = QtWidgets.QTextEdit(self.groupBox)
        self.textEdit.setReadOnly(True)
        self.textEdit.setObjectName("textEdit")
        self.verticalLayout_2.addWidget(self.textEdit)
        self.pushButton = QtWidgets.QPushButton(self.groupBox)
        self.pushButton.setObjectName("pushButton")
        self.verticalLayout_2.addWidget(self.pushButton, 0, QtCore.Qt.AlignRight)
        self.verticalLayout.addWidget(self.groupBox)

        self.retranslateUi(SimulationSolver)
        QtCore.QMetaObject.connectSlotsByName(SimulationSolver)

    def retranslateUi(self, SimulationSolver):
        _translate = QtCore.QCoreApplication.translate
        SimulationSolver.setWindowTitle(_translate("SimulationSolver", "Form"))
        self.pushButton.setText(_translate("SimulationSolver", "Cancel"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    SimulationSolver = QtWidgets.QWidget()
    ui = Ui_SimulationSolver()
    ui.setupUi(SimulationSolver)
    SimulationSolver.show()
    sys.exit(app.exec_())
