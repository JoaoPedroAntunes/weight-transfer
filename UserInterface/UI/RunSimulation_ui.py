# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'RunSimulation_ui.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtWidgets


class Ui_RunSimulation(object):
    def setupUi(self, RunSimulation):
        RunSimulation.setObjectName("RunSimulation")
        RunSimulation.setWindowModality(QtCore.Qt.WindowModal)
        RunSimulation.resize(640, 480)
        self.verticalLayout = QtWidgets.QVBoxLayout(RunSimulation)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(RunSimulation)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.verticalLayout.addWidget(self.groupBox)
        self.groupBox_2 = QtWidgets.QGroupBox(RunSimulation)
        self.groupBox_2.setMaximumSize(QtCore.QSize(16777215, 50))
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.Label_Obj_Name = QtWidgets.QLabel(self.groupBox_2)
        self.Label_Obj_Name.setObjectName("Label_Obj_Name")
        self.horizontalLayout.addWidget(self.Label_Obj_Name, 0, QtCore.Qt.AlignRight)
        self.lineEdit_Name = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_Name.setObjectName("lineEdit_Name")
        self.horizontalLayout.addWidget(self.lineEdit_Name, 0, QtCore.Qt.AlignRight)
        self.pushButton_OK = QtWidgets.QPushButton(self.groupBox_2)
        self.pushButton_OK.setObjectName("pushButton_OK")
        self.horizontalLayout.addWidget(self.pushButton_OK, 0, QtCore.Qt.AlignRight)
        self.verticalLayout.addWidget(self.groupBox_2)

        self.retranslateUi(RunSimulation)
        QtCore.QMetaObject.connectSlotsByName(RunSimulation)

    def retranslateUi(self, RunSimulation):
        _translate = QtCore.QCoreApplication.translate
        RunSimulation.setWindowTitle(_translate("RunSimulation", "Form"))
        self.Label_Obj_Name.setText(_translate("RunSimulation", "Name:"))
        self.pushButton_OK.setText(_translate("RunSimulation", "OK"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    RunSimulation = QtWidgets.QWidget()
    ui = Ui_RunSimulation()
    ui.setupUi(RunSimulation)
    RunSimulation.show()
    sys.exit(app.exec_())
