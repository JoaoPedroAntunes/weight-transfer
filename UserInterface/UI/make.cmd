:: Remove it printing out every step
@echo off & setlocal

pyuic5 -x aerodynamics_ui.ui -o aerodynamics_ui.py
pyuic5 -x ARB_ui.ui -o ARB_ui.py
pyuic5 -x basic_properties_ui.ui -o basic_properties_ui.py
pyuic5 -x spring_ui.ui -o spring_ui.py
pyuic5 -x HalfSuspension_ui.ui -o HalfSuspension_ui.py
pyuic5 -x Tire_ui.ui -o Tire_ui.py
pyuic5 -x Vehicle_ui.ui -o Vehicle_ui.py
pyuic5 -x mainwindow.ui -o mainWindow_UI.py
pyuic5 -x Inputs_ui.ui -o Inputs_ui.py
pyuic5 -x RunSimulation_ui.ui -o RunSimulation_ui.py
pyuic5 -x SimulationSolver_ui.ui -o SimulationSolver_ui.py

Echo Done