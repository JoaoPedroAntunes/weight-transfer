from UserInterface.UI.ARB_ui import Ui_ARB
from UserInterface.UI.HalfSuspension_ui import Ui_HalfSuspension
from UserInterface.UI.Inputs_ui import Ui_Inputs
from UserInterface.UI.Spring_ui import Ui_Spring
from UserInterface.UI.Tire_ui import Ui_Tire
from UserInterface.UI.Vehicle_ui import Ui_Vehicle
from UserInterface.UI.aerodynamics_ui import Ui_Aerodynamics
from UserInterface.UI.basic_properties_ui import Ui_BasicPropertiesForm
