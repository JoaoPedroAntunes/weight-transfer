# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'HalfSuspension_ui.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtWidgets


class Ui_HalfSuspension(object):
    def setupUi(self, HalfSuspension):
        HalfSuspension.setObjectName("HalfSuspension")
        HalfSuspension.resize(640, 480)
        self.verticalLayout = QtWidgets.QVBoxLayout(HalfSuspension)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(HalfSuspension)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.frame_4 = QtWidgets.QFrame(self.groupBox)
        self.frame_4.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.gridLayout = QtWidgets.QGridLayout(self.frame_4)
        self.gridLayout.setObjectName("gridLayout")
        self.label_track = QtWidgets.QLabel(self.frame_4)
        self.label_track.setObjectName("label_track")
        self.gridLayout.addWidget(self.label_track, 0, 0, 1, 1)
        self.label = QtWidgets.QLabel(self.frame_4)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 1, 2, 1, 1)
        self.label_roll_center = QtWidgets.QLabel(self.frame_4)
        self.label_roll_center.setObjectName("label_roll_center")
        self.gridLayout.addWidget(self.label_roll_center, 1, 0, 1, 1)
        self.lineEdit_track = QtWidgets.QLineEdit(self.frame_4)
        self.lineEdit_track.setMaximumSize(QtCore.QSize(75, 16777215))
        self.lineEdit_track.setAlignment(QtCore.Qt.AlignCenter)
        self.lineEdit_track.setObjectName("lineEdit_track")
        self.gridLayout.addWidget(self.lineEdit_track, 0, 1, 1, 1, QtCore.Qt.AlignTop)
        self.lineEdit_roll_center = QtWidgets.QLineEdit(self.frame_4)
        self.lineEdit_roll_center.setMaximumSize(QtCore.QSize(75, 16777215))
        self.lineEdit_roll_center.setObjectName("lineEdit_roll_center")
        self.gridLayout.addWidget(self.lineEdit_roll_center, 1, 1, 1, 1)
        self.label_12 = QtWidgets.QLabel(self.frame_4)
        self.label_12.setObjectName("label_12")
        self.gridLayout.addWidget(self.label_12, 0, 2, 1, 1)
        self.label_non_suspended_mass_CG = QtWidgets.QLabel(self.frame_4)
        self.label_non_suspended_mass_CG.setObjectName("label_non_suspended_mass_CG")
        self.gridLayout.addWidget(self.label_non_suspended_mass_CG, 2, 0, 1, 1)
        self.lineEdit_non_suspended_mass_CG = QtWidgets.QLineEdit(self.frame_4)
        self.lineEdit_non_suspended_mass_CG.setMaximumSize(QtCore.QSize(75, 16777215))
        self.lineEdit_non_suspended_mass_CG.setObjectName("lineEdit_non_suspended_mass_CG")
        self.gridLayout.addWidget(self.lineEdit_non_suspended_mass_CG, 2, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.frame_4)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 2, 2, 1, 1)
        self.horizontalLayout_2.addWidget(self.frame_4, 0, QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.frame_5 = QtWidgets.QFrame(self.groupBox)
        self.frame_5.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_5.setObjectName("frame_5")
        self.line = QtWidgets.QFrame(self.frame_5)
        self.line.setGeometry(QtCore.QRect(10, 162, 394, 16))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.horizontalLayout_2.addWidget(self.frame_5)
        self.verticalLayout.addWidget(self.groupBox)
        self.groupBox_2 = QtWidgets.QGroupBox(HalfSuspension)
        self.groupBox_2.setMaximumSize(QtCore.QSize(16777215, 50))
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.Label_Obj_Name = QtWidgets.QLabel(self.groupBox_2)
        self.Label_Obj_Name.setObjectName("Label_Obj_Name")
        self.horizontalLayout.addWidget(self.Label_Obj_Name, 0, QtCore.Qt.AlignRight)
        self.lineEdit_Name = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_Name.setObjectName("lineEdit_Name")
        self.horizontalLayout.addWidget(self.lineEdit_Name, 0, QtCore.Qt.AlignRight)
        self.pushButton_OK = QtWidgets.QPushButton(self.groupBox_2)
        self.pushButton_OK.setObjectName("pushButton_OK")
        self.horizontalLayout.addWidget(self.pushButton_OK, 0, QtCore.Qt.AlignRight)
        self.verticalLayout.addWidget(self.groupBox_2)

        self.retranslateUi(HalfSuspension)
        QtCore.QMetaObject.connectSlotsByName(HalfSuspension)

    def retranslateUi(self, HalfSuspension):
        _translate = QtCore.QCoreApplication.translate
        HalfSuspension.setWindowTitle(_translate("HalfSuspension", "Form"))
        self.label_track.setText(_translate("HalfSuspension", "Track"))
        self.label.setText(_translate("HalfSuspension", "TextLabel"))
        self.label_roll_center.setText(_translate("HalfSuspension", "Roll Center"))
        self.label_12.setText(_translate("HalfSuspension", "TextLabel"))
        self.label_non_suspended_mass_CG.setText(_translate("HalfSuspension", "Non Suspended Mass CG"))
        self.label_3.setText(_translate("HalfSuspension", "TextLabel"))
        self.Label_Obj_Name.setText(_translate("HalfSuspension", "Name:"))
        self.pushButton_OK.setText(_translate("HalfSuspension", "OK"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    HalfSuspension = QtWidgets.QWidget()
    ui = Ui_HalfSuspension()
    ui.setupUi(HalfSuspension)
    HalfSuspension.show()
    sys.exit(app.exec_())
