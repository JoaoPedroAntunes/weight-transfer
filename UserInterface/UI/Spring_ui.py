# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'spring_ui.ui'
#
# Created by: PyQt5 UI code generator 5.13.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtWidgets


class Ui_Spring(object):
    def setupUi(self, Spring):
        Spring.setObjectName("Spring")
        Spring.resize(640, 480)
        self.verticalLayout = QtWidgets.QVBoxLayout(Spring)
        self.verticalLayout.setObjectName("verticalLayout")
        self.groupBox = QtWidgets.QGroupBox(Spring)
        self.groupBox.setObjectName("groupBox")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout(self.groupBox)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.frame_4 = QtWidgets.QFrame(self.groupBox)
        self.frame_4.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_4.setObjectName("frame_4")
        self.gridLayout = QtWidgets.QGridLayout(self.frame_4)
        self.gridLayout.setObjectName("gridLayout")
        self.label_stiffness = QtWidgets.QLabel(self.frame_4)
        self.label_stiffness.setObjectName("label_stiffness")
        self.gridLayout.addWidget(self.label_stiffness, 0, 0, 1, 1)
        self.lineEdit_stiffness = QtWidgets.QLineEdit(self.frame_4)
        self.lineEdit_stiffness.setMaximumSize(QtCore.QSize(75, 16777215))
        self.lineEdit_stiffness.setAlignment(QtCore.Qt.AlignCenter)
        self.lineEdit_stiffness.setObjectName("lineEdit_stiffness")
        self.gridLayout.addWidget(self.lineEdit_stiffness, 0, 1, 1, 1, QtCore.Qt.AlignTop)
        self.label_motion_ratio = QtWidgets.QLabel(self.frame_4)
        self.label_motion_ratio.setObjectName("label_motion_ratio")
        self.gridLayout.addWidget(self.label_motion_ratio, 1, 0, 1, 1)
        self.label_12 = QtWidgets.QLabel(self.frame_4)
        self.label_12.setObjectName("label_12")
        self.gridLayout.addWidget(self.label_12, 0, 2, 1, 1)
        self.lineEdit_motion_ratio = QtWidgets.QLineEdit(self.frame_4)
        self.lineEdit_motion_ratio.setMaximumSize(QtCore.QSize(75, 16777215))
        self.lineEdit_motion_ratio.setAlignment(QtCore.Qt.AlignCenter)
        self.lineEdit_motion_ratio.setObjectName("lineEdit_motion_ratio")
        self.gridLayout.addWidget(self.lineEdit_motion_ratio, 1, 1, 1, 1)
        self.label_3 = QtWidgets.QLabel(self.frame_4)
        self.label_3.setObjectName("label_3")
        self.gridLayout.addWidget(self.label_3, 1, 2, 1, 1)
        self.horizontalLayout_2.addWidget(self.frame_4, 0, QtCore.Qt.AlignLeft | QtCore.Qt.AlignTop)
        self.frame_5 = QtWidgets.QFrame(self.groupBox)
        self.frame_5.setFrameShape(QtWidgets.QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QtWidgets.QFrame.Raised)
        self.frame_5.setObjectName("frame_5")
        self.line = QtWidgets.QFrame(self.frame_5)
        self.line.setGeometry(QtCore.QRect(10, 162, 394, 16))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.horizontalLayout_2.addWidget(self.frame_5)
        self.verticalLayout.addWidget(self.groupBox)
        self.groupBox_2 = QtWidgets.QGroupBox(Spring)
        self.groupBox_2.setMaximumSize(QtCore.QSize(16777215, 50))
        self.groupBox_2.setObjectName("groupBox_2")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.groupBox_2)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout.addItem(spacerItem)
        self.Label_Obj_Name = QtWidgets.QLabel(self.groupBox_2)
        self.Label_Obj_Name.setObjectName("Label_Obj_Name")
        self.horizontalLayout.addWidget(self.Label_Obj_Name, 0, QtCore.Qt.AlignRight)
        self.lineEdit_Name = QtWidgets.QLineEdit(self.groupBox_2)
        self.lineEdit_Name.setObjectName("lineEdit_Name")
        self.horizontalLayout.addWidget(self.lineEdit_Name, 0, QtCore.Qt.AlignRight)
        self.pushButton_OK = QtWidgets.QPushButton(self.groupBox_2)
        self.pushButton_OK.setObjectName("pushButton_OK")
        self.horizontalLayout.addWidget(self.pushButton_OK, 0, QtCore.Qt.AlignRight)
        self.verticalLayout.addWidget(self.groupBox_2)

        self.retranslateUi(Spring)
        QtCore.QMetaObject.connectSlotsByName(Spring)

    def retranslateUi(self, Spring):
        _translate = QtCore.QCoreApplication.translate
        Spring.setWindowTitle(_translate("Spring", "Form"))
        self.label_stiffness.setText(_translate("Spring", "Stiffness"))
        self.label_motion_ratio.setText(_translate("Spring", "Motion Ratio"))
        self.label_12.setText(_translate("Spring", "N/m"))
        self.label_3.setText(_translate("Spring", "TextLabel"))
        self.Label_Obj_Name.setText(_translate("Spring", "Name:"))
        self.pushButton_OK.setText(_translate("Spring", "OK"))


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Spring = QtWidgets.QWidget()
    ui = Ui_Spring()
    ui.setupUi(Spring)
    Spring.show()
    sys.exit(app.exec_())
