import os
import sys
from pathlib import Path

from PyQt5 import QtWidgets, QtCore

from Design import BasicProperties_D, Inputs_D, Spring_D, ARB_D, Tire_D, Aerodynamics_D
from Design.Results_D import Results_D
from Globals import Globals
from Managers.ProjectFramework import ProjectFramework
from Managers.TreeResultsManager import TreeResultsManager
from Managers.TreeWidgetManager import TreeWidgetManager
from UserInterface.Simulation.GetUserSimulation_W import GetUserSimulation_W
from UserInterface.Simulation.RunSimulation_W import RunSimulation_W
from UserInterface.UI.MainWindow_UI import Ui_MainWindow


class MainWindow_W(ProjectFramework):
    def __init__(self):
        self.app = QtWidgets.QApplication(sys.argv)
        self.MainWindow = QtWidgets.QMainWindow()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self.MainWindow)

        # Create the project Tree Widget
        self.ui.treeWidget_project = TreeWidgetManager(self.ui.tab)
        self.ui.treeWidget_project.setObjectName("treeWidget_project")
        self.ui.treeWidget_project.headerItem().setText(0, "1")
        self.ui.treeWidget_project.header().setVisible(False)
        self.ui.verticalLayout.addWidget(self.ui.treeWidget_project)

        # Create the results tree Widget
        self.ui.treeWidget_results = TreeResultsManager(self.ui.tab_2)
        self.ui.treeWidget_results.setObjectName("treeWidget_results")
        self.ui.treeWidget_results.headerItem().setText(0, "1")
        self.ui.treeWidget_results.header().setVisible(False)
        self.ui.verticalLayout_2.addWidget(self.ui.treeWidget_results)

        ProjectFramework.__init__(self, "Test", ".wt", "0.0.0")


        # Connect the events from the Menu bar
        self.ui.actionNew_Project.triggered.connect(self.create_new_project)
        self.ui.actionOpen_Project.triggered.connect(self.open_project)
        self.ui.treeWidget_project.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.ui.treeWidget_project.customContextMenuRequested.connect(self.ui.treeWidget_project.menuContextTree)
        self.ui.treeWidget_project.itemDoubleClicked.connect(self.ui.treeWidget_project.on_itemDoubleClicked)
        self.ui.actionRun.triggered.connect(self.get_simulation_and_run)

        # Connect the events for the treeWidget Results
        self.ui.treeWidget_results.itemDoubleClicked.connect(self.ui.treeWidget_results.on_itemDoubleClicked)

    def __set_project_directory__(self, path):
        self.project_directory = path
        self.ui.treeWidget_project.project_directory = path
        self.ui.treeWidget_results.project_directory = path

    def get_user_simulation(self):
        self.obj_form = GetUserSimulation_W(self.ui.treeWidget_project)
        self.obj_form.Form.exec()
        return self.obj_form.input_list, self.obj_form.vehicle_list


    def run_simulation(self, input_list_out, vehicle_list_out):
        if input_list_out and vehicle_list_out:
            self.obj_form2 = RunSimulation_W(self.project_directory, input_list_out, vehicle_list_out)
            self.obj_form2.Form.exec()

            # Todo necessary to add a (1) if the name already exists.
            # Todo create a function to add the master node
            # Create results node
            for i in range(len(input_list_out)):
                for j in range(len(vehicle_list_out)):
                    results_name = str(input_list_out[i]) + " - " + str(vehicle_list_out[j])
                    res = Results_D(results_name)
                    res.save_object(self.project_directory + "/" + Globals.TreeViewResults.results.value)
                    self.ui.treeWidget_results.add_obj_to_tree(results_name, Globals.TreeViewResults.results.value)


        else:
            # List is empty don't to anything
            return

    def get_simulation_and_run(self):

        input_list_out, vehicle_list_out = self.get_user_simulation()

        self.run_simulation(input_list_out, vehicle_list_out)


    def create_new_project(self):

        options = QtWidgets.QFileDialog.Options()
        options |= QtWidgets.QFileDialog.DontUseNativeDialog
        # TODO change the directory to be a default one from the user.
        file_path = QtWidgets.QFileDialog.getSaveFileName(None, "Create Project", str(Path.home()) + "/Documents",
                                                          "WT (*.wt)",
                                                         options=options)

        project_name = str(file_path[0]).split("/")[-1]

        self.create_project_folders(project_name, file_path[0])

        self.ui.treeWidget_project.create_master_tree_nodes(Globals.TreeViewNames)
        self.ui.treeWidget_results.create_master_tree_nodes(Globals.TreeViewResults)


    #TODO finish the event handler of having the save project. Basically if the user does Cntrl + S the project should be saved
    def save_project(self):
        pass

    # TODO have the option to save as the project basically saves everything into another directory
    # TODO Check if the project already exists before saving
    def save_as_project(self):
        pass

    # TODO load the project and all the other properties such as aero and so on
    def open_project(self, file_path_directory=""):

        # If not directory is specified in the input then create a pop-up window to ask the user for a project to open
        if file_path_directory == False:  # The False statement is necessary because when it is called by QT it returns a bolean and not a string
            options = QtWidgets.QFileDialog.Options()
            options |= QtWidgets.QFileDialog.DontUseNativeDialog
            # TODO change the directory to be a default one from the user.
            file_path_directory = QtWidgets.QFileDialog.getOpenFileName(None, "Create Project",
                                                                        "C:\\Users\\joaop\\Desktop",
                                                                        "WT (*.wt)",
                                                                        options=options)
            # Remove the second item that we don't need
            file_path_directory = file_path_directory[0]

            # If the file_path_directory is empty it means that the user canceled thus return. If it is false it will run the remaining code
            if file_path_directory == "": return

        # When the user opens a project we need to remove the previous one
        self.ui.treeWidget_project.clear()

        # Remove the last item so that we can get the project directory
        file_path_split = str(file_path_directory).split("/")
        self.__set_project_directory__("")
        for i in range(0, len(file_path_split) - 1):
            if i == 0:
                self.__set_project_directory__(file_path_split[i])
            else:
                self.__set_project_directory__(self.project_directory + "\\" + file_path_split[i])

        self.ui.treeWidget_project.create_master_tree_nodes(Globals.TreeViewNames)
        self.ui.treeWidget_results.create_master_tree_nodes(Globals.TreeViewResults)

        # Load the object from the project into the tree nodes
        self.ui.treeWidget_project.load_child_nodes(self.project_directory, Globals.TreeViewNames)
        self.ui.treeWidget_results.load_child_nodes(self.project_directory, Globals.TreeViewResults)

        self.ui.treeWidget_project.expandAll()
        self.ui.treeWidget_results.expandAll()

    def show(self):
        self.MainWindow.show()
        sys.exit(self.app.exec_())


if __name__ == "__main__":
    main_window_W = MainWindow_W()
    project_name = "Test"
    file_path = "C:\\Users\\joaop\\Desktop\\Test"

    # If folder doesn't exist create the folder and populate it with a demo project
    if not os.path.exists(file_path):
        foo = ProjectFramework()
        foo.create_project_folders(project_name, file_path)

        # Create 3 files for each folder
        name_list = ["Test1", "Test2", "Test3"]
        for name in name_list:
            aero = Aerodynamics_D()
            aero.name = name
            aero.init_default_values()
            aero.save_object(file_path + "\\" + Globals.TreeViewNames.aerodynamics.value)

            bp = BasicProperties_D()
            bp.name = name
            bp.init_default_values()
            bp.save_object(file_path + "\\" + Globals.TreeViewNames.basic_properties.value)

            inp = Inputs_D()
            inp.name = name
            inp.init_default_values()
            inp.save_object(file_path + "\\" + Globals.TreeViewNames.inputs.value)

            spring = Spring_D()
            spring.name = name
            spring.init_default_values()
            spring.save_object(file_path + "\\" + Globals.TreeViewNames.spring.value)

            arb = ARB_D()
            arb.name = name
            arb.init_default_values()
            arb.save_object(file_path + "\\" + Globals.TreeViewNames.ARB.value)

            tire = Tire_D()
            tire.name = name
            tire.init_default_values()
            tire.save_object(file_path + "\\" + Globals.TreeViewNames.tire.value)

    main_window_W.show()
