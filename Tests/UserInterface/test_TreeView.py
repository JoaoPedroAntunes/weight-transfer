import os

from PyQt5 import QtWidgets

from Design import *
from Globals import Globals
from Managers.ProjectFramework import ProjectFramework
from UserInterface.MainWindow_W import MainWindow_W

# Init Window
main_window = MainWindow_W()

# Default names
project_name = "Test"
file_path = "C:\\Users\\joaop\\Desktop\\Test"

# If folder doesn't exist create the folder and populate it with a demo project
if not os.path.exists(file_path):
    foo = ProjectFramework()
    foo.create_project_folders(project_name, file_path)

    # Create 3 files for each folder
    name_list = ["Test1", "Test2", "Test3"]
    for name in name_list:
        aero = Aerodynamics_D()
        aero.name = name
        aero.init_default_values()
        aero.save_object(file_path + "\\" + Globals.TreeViewNames.aerodynamics.value)

        bp = BasicProperties_D()
        bp.name = name
        bp.init_default_values()
        bp.save_object(file_path + "\\" + Globals.TreeViewNames.basic_properties.value)

        inp = Inputs_D()
        inp.name = name
        inp.init_default_values()
        inp.save_object(file_path + "\\" + Globals.TreeViewNames.inputs.value)

        spring = Spring_D()
        spring.name = name
        spring.init_default_values()
        spring.save_object(file_path + "\\" + Globals.TreeViewNames.spring.value)

        arb = ARB_D()
        arb.name = name
        arb.init_default_values()
        arb.save_object(file_path + "\\" + Globals.TreeViewNames.ARB.value)

        tire = Tire_D()
        tire.name = name
        tire.init_default_values()
        tire.save_object(file_path + "\\" + Globals.TreeViewNames.tire.value)

# Create the TreeView master nodes
for i, results_dic in enumerate(Globals.TreeViewNames):
    top_level_item = QtWidgets.QTreeWidgetItem(main_window.ui.treeWidget)
    top_level_item.setText(0, results_dic.value)

# Create the second nodes
for j in range(main_window.ui.treeWidget.topLevelItemCount()):

    # Aero
    if main_window.ui.treeWidget.topLevelItem(j).text(0) == Globals.TreeViewNames.aerodynamics.value:
        obj_in_folder = path = os.listdir(file_path + "\\" + Globals.TreeViewNames.aerodynamics.value)

        for name in obj_in_folder:
            sub_level_item = QtWidgets.QTreeWidgetItem(main_window.ui.treeWidget.topLevelItem(j))
            aero_obj_loaded = Aerodynamics_D()
            aero_obj_loaded.load_object(file_path + "\\" + Globals.TreeViewNames.aerodynamics.value + "\\" + name)
            sub_level_item.setText(0, aero_obj_loaded.name)

    elif main_window.ui.treeWidget.topLevelItem(j).text(0) == Globals.TreeViewNames.basic_properties.value:

        obj_in_folder = path = os.listdir(file_path + "\\" + Globals.TreeViewNames.basic_properties.value)

        for name in obj_in_folder:
            sub_level_item = QtWidgets.QTreeWidgetItem(main_window.ui.treeWidget.topLevelItem(j))
            aero_obj_loaded = Aerodynamics_D()
            aero_obj_loaded.load_object(file_path + "\\" + Globals.TreeViewNames.basic_properties.value + "\\" + name)
            sub_level_item.setText(0, aero_obj_loaded.name)

    elif main_window.ui.treeWidget.topLevelItem(j).text(0) == Globals.TreeViewNames.inputs.value:

        obj_in_folder = path = os.listdir(file_path + "\\" + Globals.TreeViewNames.inputs.value)

        for name in obj_in_folder:
            sub_level_item = QtWidgets.QTreeWidgetItem(main_window.ui.treeWidget.topLevelItem(j))
            aero_obj_loaded = Aerodynamics_D()
            aero_obj_loaded.load_object(file_path + "\\" + Globals.TreeViewNames.inputs.value + "\\" + name)
            sub_level_item.setText(0, aero_obj_loaded.name)


    elif main_window.ui.treeWidget.topLevelItem(j).text(0) == Globals.TreeViewNames.spring.value:
        obj_in_folder = path = os.listdir(file_path + "\\" + Globals.TreeViewNames.spring.value)

        for name in obj_in_folder:
            sub_level_item = QtWidgets.QTreeWidgetItem(main_window.ui.treeWidget.topLevelItem(j))
            aero_obj_loaded = Aerodynamics_D()
            aero_obj_loaded.load_object(file_path + "\\" + Globals.TreeViewNames.spring.value + "\\" + name)
            sub_level_item.setText(0, aero_obj_loaded.name)


    elif main_window.ui.treeWidget.topLevelItem(j).text(0) == Globals.TreeViewNames.ARB.value:
        obj_in_folder = path = os.listdir(file_path + "\\" + Globals.TreeViewNames.ARB.value)

        for name in obj_in_folder:
            sub_level_item = QtWidgets.QTreeWidgetItem(main_window.ui.treeWidget.topLevelItem(j))
            aero_obj_loaded = Aerodynamics_D()
            aero_obj_loaded.load_object(file_path + "\\" + Globals.TreeViewNames.ARB.value + "\\" + name)
            sub_level_item.setText(0, aero_obj_loaded.name)


    elif main_window.ui.treeWidget.topLevelItem(j).text(0) == Globals.TreeViewNames.tire.value:
        obj_in_folder = path = os.listdir(file_path + "\\" + Globals.TreeViewNames.tire.value)

        for name in obj_in_folder:
            sub_level_item = QtWidgets.QTreeWidgetItem(main_window.ui.treeWidget.topLevelItem(j))
            aero_obj_loaded = Aerodynamics_D()
            aero_obj_loaded.load_object(file_path + "\\" + Globals.TreeViewNames.tire.value + "\\" + name)
            sub_level_item.setText(0, aero_obj_loaded.name)

main_window.show()
