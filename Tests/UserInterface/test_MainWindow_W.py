from PyQt5 import QtWidgets

from Globals import Globals
from UserInterface.MainWindow_W import MainWindow_W


def test_main_window_design_tree():
    main_window = MainWindow_W()

    data = ["155", "2", "3", "4"]
    top_level_item = QtWidgets.QTreeWidgetItem(data)
    main_window.ui.treeWidget.addTopLevelItem(top_level_item)

    #  for i, results_dic in enumerate(Globals.TreeViewNames):

    #     top_level_item = QtWidgets.QTreeWidgetItem(main_window.ui.treeWidget)
    #     top_level_item.setText(0, results_dic.value)

    #    sub_level_item = QtWidgets.QTreeWidgetItem(top_level_item)
    #   sub_level_item.setText(0, "Test2")

    main_window.show()


def test_create_project():
    main_window = MainWindow_W()

    # Create the TreeView master nodes
    for i, results_dic in enumerate(Globals.TreeViewNames):
        top_level_item = QtWidgets.QTreeWidgetItem(main_window.ui.treeWidget)
        top_level_item.setText(0, results_dic.value)

    # Create the second nodes
    for j in main_window.ui.treeWidget.topLevelItemCount():
        if j == Globals.TreeViewNames.aerodynamics.value:
            pass

    main_window.show()
