
import Globals
from Design.Vehicle_D import Vehicle_D
from Solver.VehicleBatch import create_vehicle_batch
from Solver.Vehicle_S import Vehicle_S


def test_VehicleBatch_step05():
    default_vehicle_D = Vehicle_D()
    default_vehicle_D.init_default_values()

    default_vehicle_S = Vehicle_S(default_vehicle_D)

    default_vehicle_S_batch = create_vehicle_batch(Globals.VehicleParametersName.air_density.name,
                                                   default_vehicle_S,
                                                   0, 1, 0.5)

    assert len(default_vehicle_S_batch) == 3, "Should be 3"
    assert default_vehicle_S_batch[0].aerodynamics.air_density == 0, "Should be 0"
    assert default_vehicle_S_batch[1].aerodynamics.air_density == 0.5, "Should be 0.5"
    assert default_vehicle_S_batch[2].aerodynamics.air_density == 1, "Should be 1"


def test_VehicleBatch_step025():
    default_vehicle_D = Vehicle_D()
    default_vehicle_D.init_default_values()

    default_vehicle_S = Vehicle_S(default_vehicle_D)

    default_vehicle_S_batch = create_vehicle_batch(Globals.VehicleParametersName.air_density.name,
                                                   default_vehicle_S,
                                                   0, 1, 0.25)

    assert len(default_vehicle_S_batch) == 5, "Should be 5"
    assert default_vehicle_S_batch[0].aerodynamics.air_density == 0, "Should be 0"
    assert default_vehicle_S_batch[1].aerodynamics.air_density == 0.25, "Should be 0.25"
    assert default_vehicle_S_batch[2].aerodynamics.air_density == 0.5, "Should be 0.5"
    assert default_vehicle_S_batch[3].aerodynamics.air_density == 0.75, "Should be 0.75"
    assert default_vehicle_S_batch[4].aerodynamics.air_density == 1, "Should be 1"


def test_VehicleBatch_step02():
    default_vehicle_D = Vehicle_D()
    default_vehicle_D.init_default_values()

    default_vehicle_S = Vehicle_S(default_vehicle_D)

    default_vehicle_S_batch = create_vehicle_batch(Globals.VehicleParametersName.air_density.name,
                                                   default_vehicle_S,
                                                   0, 1, 0.2)

    assert len(default_vehicle_S_batch) == 6, "Should be 6"
