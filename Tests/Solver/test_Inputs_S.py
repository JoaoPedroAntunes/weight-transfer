from Design.Inputs_D import Inputs_D
from Solver.Inputs_S import Inputs_S


def test_generate_steps():
    default_input = Inputs_D()
    default_input.create_linear_lateral_acceleration_input(0,20,1)
    default_input_solver = Inputs_S(default_input)
    default_input_solver.__generate_simulation_steps__()

    assert len(default_input_solver.steps) == len(default_input_solver.lateral_acceleration), "Should be 21 steps"


def test_if_input_is_valid():
    default_input = Inputs_D()
    default_input.create_linear_lateral_acceleration_input(0,20,1)
    default_input.create_linear_longitudinal_acceleration_input(0,20,1)
    default_input.create_linear_longitudinal_velocity_input(0,20,1)
    default_input_solver = Inputs_S(default_input)
    default_input_solver.__generate_simulation_steps__()

    assert default_input_solver.__validated_input__() == True, "Should be True"


def test_step_generation():
    default_input = Inputs_D()
    default_input.create_linear_longitudinal_acceleration_input(0, 20, 1)
