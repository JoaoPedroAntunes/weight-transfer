from Design.HalfSuspension_D import HalfSuspension_D
from Design.Suspension_D import Suspension_D
from Design.Vehicle_D import Vehicle_D

# Test if vehicle is correct
default_vehicle = Vehicle_D()
default_vehicle.init_default_values()
default_vehicle.print_vehicle_properties()

default_half_suspension = HalfSuspension_D()
default_half_suspension.init_default_values()

default_suspension = Suspension_D()
default_suspension.init_default_values()


def test_aero_D():
    assert default_vehicle.aerodynamics.downforce_coefficient == 2.2, "Should be 2.2"
    assert default_vehicle.aerodynamics.frontal_area == 1.75, "Should be 1.75"
    assert default_vehicle.aerodynamics.air_density == 1.23, "Should be 1.23"
    assert default_vehicle.aerodynamics.downforce_distribution == 0.46, "Should be 0.46"


def test_basic_properties_D():
    assert default_vehicle.basic_properties.mass == 994*9.81, "Should be 994*9.81"
    assert default_vehicle.suspension.front.track == 1.640, "Should be 1.640"
    assert default_vehicle.suspension.rear.track == 1.592, "Should be 1.592"
    assert default_vehicle.basic_properties.mass_CG_height == 0.27, "Should be 0.27"
    assert default_vehicle.basic_properties.mass_distribution == 0.47, "Should be 0.47"
    assert default_vehicle.basic_properties.wheelbase == 2.980, "Should be 2.980"


def test_suspension_spring_stiffness():
    assert default_vehicle.suspension.front.left.spring.stiffness == 127.1*1000, "Should be 127.1*1000"
    assert default_vehicle.suspension.front.right.spring.stiffness == 127.1*1000, "Should be 127.1*1000"
    assert default_vehicle.suspension.rear.left.spring.stiffness == 103.4*1000, "Should be 103.4*1000"
    assert default_vehicle.suspension.rear.right.spring.stiffness == 103.4*1000, "Should be 103.4*1000"

def test_suspension_spring_motion_ratio():
    assert default_vehicle.suspension.front.left.spring.motion_ratio == 0.82, "Should be 0.82"
    assert default_vehicle.suspension.front.right.spring.motion_ratio == 0.82, "Should be 0.82"
    assert default_vehicle.suspension.rear.left.spring.motion_ratio == 0.95, "Should be 0.95"
    assert default_vehicle.suspension.rear.right.spring.motion_ratio == 0.95, "Should be 0.95"


def test_suspension_ARB_stiffness():
    assert default_vehicle.suspension.front.ARB.stiffness == 180*1000, "Should be 180*1000"
    assert  default_vehicle.suspension.rear.ARB.stiffness == 25.4*1000, "Should be 25.4*1000"


def test_suspension_ARB_motion_ratio():
    assert default_vehicle.suspension.front.ARB.motion_ratio == 1.15, "Should be 1.15"
    assert default_vehicle.suspension.rear.ARB.motion_ratio == 1.03, "Should be 1.03"


def test_roll_center():
    assert default_vehicle.suspension.front.roll_center.z == 0.05, "Should be 0.05"
    assert default_vehicle.suspension.rear.roll_center.z == 0.08, "Should be 0.08"


def test_non_suspended_mass():
    assert default_vehicle.suspension.front.left.non_suspended_mass == 38*9.81, "Should be 38*9.81"
    assert default_vehicle.suspension.front.right.non_suspended_mass == 38*9.81, "Should be 38*9.81"
    assert default_vehicle.suspension.rear.left.non_suspended_mass == 42*9.81, "Should be 42*9.81"
    assert default_vehicle.suspension.rear.right.non_suspended_mass == 42*9.81, "Should be 42*9.81"

    assert default_vehicle.suspension.front.non_susp_mass_CG == 0.330, "Should be 0.330"
    assert default_vehicle.suspension.rear.non_susp_mass_CG == 0.355, "Should be 0.355"

