import os

from Design.ARB_D import ARB_D
from Design.Aerodynamics_D import Aerodynamics_D
from Design.BasicProperties_D import BasicProperties_D
from Design.Spring_D import Spring_D


def test_load_save_Aerodynamics_D():
    obj_save = Aerodynamics_D()
    obj_save.init_default_values()
    obj_save.name = "Aero1"
    obj_save.save_object()

    obj_load = Aerodynamics_D()
    obj_load.load_object(obj_save.name + obj_save.extension)

    os.remove(obj_save.name + obj_save.extension)

    assert obj_load.name == obj_save.name, "Should be equal"
    assert obj_load.downforce_coefficient == 2.2, "Should be 2.2"
    assert obj_load.frontal_area == 1.75, "Should be 1.75"
    assert obj_load.air_density == 1.23, "Should be 1.23"
    assert obj_load.downforce_distribution == 0.46, "Should be 0.46"


def test_load_save_BasicProperties_D():
    obj_save = BasicProperties_D()
    obj_save.init_default_values()
    obj_save.name = "Basic1"
    obj_save.save_object()

    obj_load = BasicProperties_D()
    obj_load.load_object(obj_save.name + obj_save.extension)

    os.remove(obj_save.name + obj_save.extension)

    assert obj_load.weight == 994 * 9.81, "Should be 994*9.81"
    assert obj_load.suspended_weight_CG == 0.27, "Should be 0.27"
    assert obj_load.suspended_weight_distribution == 0.47, "Should be 0.47"
    assert obj_load.wheelbase == 2.980, "Should be 2.980"


def test_load_save_Spring_D():
    obj_save = Spring_D()
    obj_save.init_default_values()
    obj_save.name = "Spring1"
    obj_save.save_object()

    obj_load = Spring_D()
    obj_load.load_object(obj_save.name + obj_save.extension)

    os.remove(obj_save.name + obj_save.extension)

    assert obj_load.stiffness == 127.1 * 1000, "Should be 127.1*1000"
    assert obj_load.motion_ratio == 0.82, "Should be 0.82"


def test_load_save_ARB_D():
    obj_save = ARB_D()
    obj_save.init_default_values()
    obj_save.name = "Spring1"
    obj_save.save_object()

    obj_load = ARB_D()
    obj_load.load_object(obj_save.name + obj_save.extension)

    os.remove(obj_save.name + obj_save.extension)


    assert obj_load.stiffness == 180*1000, "Should be 180*1000"
    assert obj_load.motion_ratio == 1.15, "Should be 1.15"