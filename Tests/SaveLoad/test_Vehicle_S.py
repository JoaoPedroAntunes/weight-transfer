from Design.Vehicle_D import Vehicle_D
from Solver.Vehicle_S import Vehicle_S

# Create default vehicle
default_vehicle_D = Vehicle_D()
default_vehicle_D.init_default_values()

# Create Solver
default_vehicle = Vehicle_S(default_vehicle_D)

def test_vehicle_S_obj():
    # AERO
    assert default_vehicle.aerodynamics.downforce_coefficient == 2.2, "Should be 2.2"
    assert default_vehicle.aerodynamics.frontal_area == 1.75, "Should be 1.75"
    assert default_vehicle.aerodynamics.air_density == 1.23, "Should be 1.23"
    assert default_vehicle.aerodynamics.downforce_distribution == 0.46, "Should be 0.46"

    #Basic Properties
    assert default_vehicle.basic_properties.weight == 994 * 9.81, "Should be 994*9.81"
    assert default_vehicle.suspension.front.track == 1.640, "Should be 1.640"
    assert default_vehicle.suspension.rear.track == 1.592, "Should be 1.592"
    assert default_vehicle.basic_properties.suspended_weight_CG == 0.27, "Should be 0.27"
    assert default_vehicle.basic_properties.suspended_weight_distribution == 0.47, "Should be 0.47"
    assert default_vehicle.basic_properties.wheelbase == 2.980, "Should be 2.980"

    # Spring stiffness
    assert default_vehicle.suspension.front.left.spring.stiffness == 127.1*1000, "Should be 127.1*1000"
    assert default_vehicle.suspension.front.right.spring.stiffness == 127.1*1000, "Should be 127.1*1000"
    assert default_vehicle.suspension.rear.left.spring.stiffness == 103.4*1000, "Should be 103.4*1000"
    assert default_vehicle.suspension.rear.right.spring.stiffness == 103.4*1000, "Should be 103.4*1000"

    #Spring motion Ratio
    assert default_vehicle.suspension.front.left.spring.motion_ratio == 0.82, "Should be 0.82"
    assert default_vehicle.suspension.front.right.spring.motion_ratio == 0.82, "Should be 0.82"
    assert default_vehicle.suspension.rear.left.spring.motion_ratio == 0.95, "Should be 0.95"
    assert default_vehicle.suspension.rear.right.spring.motion_ratio == 0.95, "Should be 0.95"

    #ARB stiffness
    assert default_vehicle.suspension.front.ARB.stiffness == 180*1000, "Should be 180*1000"
    assert  default_vehicle.suspension.rear.ARB.stiffness == 25.4*1000, "Should be 25.4*1000"

    #ARB motion ratio
    assert default_vehicle.suspension.front.ARB.motion_ratio == 1.15, "Should be 1.15"
    assert default_vehicle.suspension.rear.ARB.motion_ratio == 1.03, "Should be 1.03"

    #Roll Center
    assert default_vehicle.suspension.front.roll_center == 0.05, "Should be 0.05"
    assert default_vehicle.suspension.rear.roll_center == 0.08, "Should be 0.08"

    # Non suspended weight
    assert default_vehicle.suspension.front.left.non_suspended_mass == 38*9.81, "Should be 38*9.81"
    assert default_vehicle.suspension.front.right.non_suspended_mass == 38*9.81, "Should be 38*9.81"
    assert default_vehicle.suspension.rear.left.non_suspended_mass == 42*9.81, "Should be 42*9.81"
    assert default_vehicle.suspension.rear.right.non_suspended_mass == 42*9.81, "Should be 42*9.81"

    #non suspended massCG
    assert default_vehicle.suspension.front.non_susp_mass_CG == 0.330, "Should be 0.330"
    assert default_vehicle.suspension.rear.non_susp_mass_CG == 0.355, "Should be 0.355"

    #Anti Roll Moment
    assert round(default_vehicle.suspension.front.calculate_spring_anti_roll_moment(), 2) == 4437.08
    assert round(default_vehicle.suspension.front.calculate_ARB_anti_roll_moment(), 2) == 6389.78

    assert round(default_vehicle.suspension.rear.calculate_spring_anti_roll_moment(), 2) == 2534.26
    assert round(default_vehicle.suspension.rear.calculate_ARB_anti_roll_moment(), 2) == 1059.17
