import matplotlib.pyplot as plt

import Globals
from Design.Inputs_D import Inputs_D
from Design.Vehicle_D import Vehicle_D
from Solver.Inputs_S import Inputs_S
from Solver.VehicleBatch import create_vehicle_batch
from Solver.Vehicle_S import Vehicle_S
from WeightTransferSimulation import WeightTransferSimulation


def test_lat_acc_ARB_MR():
    # Define Vehicle
    default_vehicle_D = Vehicle_D()
    default_vehicle_D.init_default_values()

    # Pass Vehicle Solver
    default_vehicle_S = Vehicle_S(default_vehicle_D)

    # Create Batch Vehicles
    default_vehicle_S_batch = create_vehicle_batch(Globals.VehicleParametersName.front_ARB_motion_ratio.name,
                                                   default_vehicle_S,
                                                   10, 200, 20)

    # Create Inputs
    lat_acc = Inputs_D()
    lat_acc.create_linear_lateral_acceleration_input(20, 20, 1)  # 2g simulation
    lat_acc.create_linear_longitudinal_velocity_input(20, 20, 1)  # 2g simulation
    lat_acc.create_linear_longitudinal_acceleration_input(20, 20, 1)  # 2g simulation

    lat_acc_S = Inputs_S(lat_acc)

    sim = WeightTransferSimulation(default_vehicle_S_batch, [lat_acc_S])
    sim.run()

    res = sim.get_simulation_results()

    arb = []
    elastic_WT = []
    geo_WT = []
    non_susp_WT = []
    for i in range(len(default_vehicle_S_batch)):
        arb.append(default_vehicle_S_batch[i].suspension.front.ARB.motion_ratio)
        elastic_WT.append(res[i].total_elastic_WT)
        geo_WT.append(res[i].total_geometric_WT)
        non_susp_WT.append(res[i].total_non_susp_mass_WT)

    fig, ax = plt.subplots()
    ax.plot(arb, elastic_WT)
    ax.plot(arb, geo_WT)
    ax.plot(arb, non_susp_WT)
    plt.xlabel("Front ARB Motion Ratio")
    plt.ylabel("Weight Transfer [N]")
    plt.legend(["Elastic", "Geometric", "Non Suspended"])
    ax.grid(True)
    plt.show()

def test_lat_acc_ARB_stiffness():
    # Define Vehicle
    default_vehicle_D = Vehicle_D()
    default_vehicle_D.init_default_values()

    # Pass Vehicle Solver
    default_vehicle_S = Vehicle_S(default_vehicle_D)

    # Create Batch Vehicles
    default_vehicle_S_batch = create_vehicle_batch(Globals.VehicleParametersName.front_ARB_stiffness.name,
                                                   default_vehicle_S,
                                                   10000, 200000, 20)

    # Create Inputs
    lat_acc = Inputs_D()
    lat_acc.create_linear_lateral_acceleration_input(20, 20, 1)  # 2g simulation
    lat_acc.create_linear_longitudinal_velocity_input(20, 20, 1)  # 2g simulation
    lat_acc.create_linear_longitudinal_acceleration_input(20, 20, 1)  # 2g simulation

    lat_acc_S = Inputs_S(lat_acc)

    sim = WeightTransferSimulation(default_vehicle_S_batch, [lat_acc_S])
    sim.run()

    res = sim.get_simulation_results()

    arb = []
    front_elastic_WT = []
    front_geo_WT = []
    front_non_susp_WT = []
    for i in range(len(default_vehicle_S_batch)):
        arb.append(default_vehicle_S_batch[i].suspension.front.ARB.stiffness)
        front_elastic_WT.append(res[i].elastic_WT_front)
        front_geo_WT.append(res[i].geometric_WT_front)
        front_non_susp_WT.append(res[i].non_susp_mass_front_WT)

    fig, ax = plt.subplots()
    ax.plot(arb, front_elastic_WT)
    ax.plot(arb, front_geo_WT)
    ax.plot(arb, front_non_susp_WT)
    plt.xlabel("Front ARB Stiffness [N/m]")
    plt.ylabel("Weight Transfer [N]")
    plt.legend(["Front Elastic", " Front Geometric", " Front Non Suspended"])
    ax.grid(True)
    plt.show()


def test_lat_acc_rear_ARB_stiffness():
    # Define Vehicle
    default_vehicle_D = Vehicle_D()
    default_vehicle_D.init_default_values()

    # Create Batch Vehicles
    default_vehicle_S_batch = create_vehicle_batch(Globals.VehicleParametersName.rear_ARB_stiffness.name,
                                                   default_vehicle_D,
                                                   10000, 200000, 20)

    # Create Inputs
    lat_acc = Inputs_D()
    lat_acc.create_linear_lateral_acceleration_input(20, 20, 1)  # 2g simulation

    sim = WeightTransferSimulation(default_vehicle_S_batch, lat_acc)
    sim.run()

    res = sim.get_simulation_results()

    arb = []
    front_elastic_WT = []
    front_geo_WT = []
    front_non_susp_WT = []
    rear_total_anti_roll_moment = []
    for i in range(len(default_vehicle_S_batch)):
        arb.append(default_vehicle_S_batch[i].suspension.rear.ARB.stiffness)
        front_elastic_WT.append(res[i].elastic_WT_front)
        front_geo_WT.append(res[i].geometric_WT_front)
        front_non_susp_WT.append(res[i].non_susp_mass_front_WT)
        rear_total_anti_roll_moment.append(res[i].rear_total_anti_roll_moment)

    # Todo Generate all the charts in one table
    plt.subplot(2, 1, 1)
    plt.plot(arb, front_elastic_WT)
    plt.plot(arb, front_geo_WT)
    plt.plot(arb, front_non_susp_WT)
    plt.xlabel("Rear ARB Stiffness [N/m]")
    plt.ylabel("Weight Transfer [N]")
    plt.legend(["Front Elastic", " Front Geometric", " Front Non Suspended"])
    plt.grid(True)

    plt.subplot(2, 1, 2)
    plt.plot(arb, rear_total_anti_roll_moment)
    plt.xlabel("Rear ARB Stiffness [N/m]")
    plt.ylabel("Anti Roll Moment [Nm]")
    plt.legend(["Rear Anti Roll Moment"])
    plt.grid(True)
    plt.show()


def test_lat_acc_rear_spring_stiffness():
    # Define Vehicle
    default_vehicle_D = Vehicle_D()
    default_vehicle_D.init_default_values()

    # Create Batch Vehicles
    default_vehicle_D_batch = create_vehicle_batch(Globals.VehicleParametersName.spring_stiffness_FR.name,
                                                   default_vehicle_D,
                                                   10000, 200000, 20)

    # Create Inputs
    lat_acc = Inputs_D()
    lat_acc.create_linear_lateral_acceleration_input(20, 20, 1)  # 2g simulation

    sim = WeightTransferSimulation(default_vehicle_D_batch, lat_acc)
    sim.run()

    resul = sim.get_simulation_results()

    arb = []
    front_elastic_WT = []
    front_geo_WT = []
    front_non_susp_WT = []
    rear_total_anti_roll_moment = []
    for i in range(len(default_vehicle_D_batch)):
        arb.append(default_vehicle_D_batch[i].suspension.front.right.spring.stiffness)
        front_elastic_WT.append(resul[i].elastic_WT_front)
        front_geo_WT.append(resul[i].geometric_WT_front)
        front_non_susp_WT.append(resul[i].non_susp_mass_front_WT)
        rear_total_anti_roll_moment.append(resul[i].rear_total_anti_roll_moment)

    # Todo Generate all the charts in one table
    plt.subplot(2, 1, 1)
    plt.plot(arb, front_elastic_WT)
    plt.plot(arb, front_geo_WT)
    plt.plot(arb, front_non_susp_WT)
    plt.xlabel("Rear ARB Stiffness [N/m]")
    plt.ylabel("Weight Transfer [N]")
    plt.legend(["Front Elastic", " Front Geometric", " Front Non Suspended"])
    plt.grid(True)
    plt.show()
