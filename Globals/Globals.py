from enum import Enum

class VehicleParametersName(Enum):
    #Aero
    air_density = "Air Density"
    downforce_distribution = "Downforce Distribution"
    frontal_area = "Frontal Area"
    downforce_coefficient = "Downforce Coefficient"

    #Basic Properties
    total_weight = "Total Weight"
    wheelbase = "Wheelbase"
    front_track = "Front Track"
    rear_track = "Rear Track"
    weight_distribution = "Weight Distribution"
    total_weight_CG_height = "Total Weight CG Height"

    #ARB
    front_ARB_stiffness = "Front ARB Stiffness"
    front_ARB_motion_ratio = "Front ARB Motion Ratio"
    rear_ARB_stiffness = "Rear ARB Stiffness"
    rear_ARB_motion_ratio = "Rear ARB Motion Ratio"


    #Todo Finish the spring as the ARB above
    #Spring
    spring_stiffness_FL = "[FL] Spring Stiffness"
    spring_motio_ratio_FL = "[FL] Spring Motion Ratio"
    spring_stiffness_FR = "[FR] Spring Stiffness"
    spring_motio_ratio_FR = "[FR] Spring Motion Ratio"
    spring_stiffness_RL = "[RL] Spring Stiffness"
    spring_motio_ratio_RL = "[RL] Spring Motion Ratio"
    spring_stiffness_RR = "[RR] Spring Stiffness"
    spring_motio_ratio_RR = "[RR] Spring Motion Ratio"

    #TODO Finish the tire as the ARB Above
    #Tire
    tire_stiffness_FL = "[FL] Tire Stiffness"
    tire_stiffness_FR = "[FR] Tire Stiffness"
    tire_stiffness_RL = "[RL] Tire Stiffness"
    tire_stiffness_RR = "[RR] Tire Stiffness"

    #TODO Finisht eh suspension by adding front and rear
    #Suspension
    front_roll_center = "Front Roll Center"
    rear_roll_center = "Rear Roll Center"
    front_pitch_center = "Front Pitch Center"
    rear_pitch_center = "Rear Pitch Center"
    non_susp_weight_CG = "Non Suspended Weight CG"


class TreeViewNames(Enum):
    inputs = "Inputs"
    aerodynamics = "Aerodynamics"
    basic_properties = "Basic Properties"
    ARB = "ARB"
    spring = "Springs"
    tire = "Tires"
    half_suspension = "Suspension"
    vehicle = "Vehicle"


class TreeViewResults(Enum):
    results = "Results"
